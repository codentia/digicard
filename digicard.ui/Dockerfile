﻿FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
ARG BUILD_CONFIG
WORKDIR /app

# copy csproj and restore as distinct layers
#COPY digicard.ui.sln .
COPY digicard.ui/digicard.ui.csproj digicard.ui/
COPY digicard.lib/digicard.lib.csproj digicard.lib/
#COPY digicard.db/digicard.db.mdproj digicard.db/
RUN dotnet restore digicard.ui/

# copy everything else and build app
COPY digicard.ui ./digicard.ui
COPY digicard.lib ./digicard.lib

WORKDIR /app/digicard.ui
RUN dotnet publish -c $BUILD_CONFIG -o out

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime
ENV TZ=Europe/London
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
WORKDIR /app
#RUN apt-get update -qq
#RUN apt-get install -y libgdiplus
COPY --from=build /app/digicard.ui/out ./
COPY --from=build /app/digicard.ui/assets ./assets
ENTRYPOINT ["dotnet", "digicard.ui.dll"]
