﻿namespace digicard.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Options;
    using digicard.Components.API;
    using digicard.Components.UI;
    using digicard.Models.UI.Page.Subscription;

    [Authorize]
    public class SubscriptionsController : DigicardUIController
    {
        public SubscriptionsController(IWebHostEnvironment environment, IOptions<AppSettings> appSettings, IServiceProvider serviceProvider)
            : base(environment, appSettings, serviceProvider)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            SubscriptionIndexPageModel model = new SubscriptionIndexPageModel();
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Subscribe()
        {
            await Task.Delay(0);

            // TODO(TS): Return braintree token
            return Json("Not Yet Implemented");
        }

        [HttpPost]
        public async Task<IActionResult> Subscribe(string changeThis)
        {
            await Task.Delay(0);

            // TODO(TS): Submit subscription details
            return Json("Not Yet Implemented");
        }

        [HttpPost]
        public async Task<IActionResult> Cancel()
        {
            await Task.Delay(0);

            // TODO(TS): Cancel subscription
            return Json("Not Yet Implemented");
        }
    }
}
