using System.Threading.Tasks;
namespace digicard.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using digicard.Components;
    using digicard.Components.API;
    using digicard.Components.Shared.API;
    using digicard.Components.UI;
    using digicard.Components.UI.Accounts;
    using digicard.Components.UI.Cards;
    using digicard.Components.UI.Users;
    using digicard.Components.UI.Authentication;
    using digicard.Mappers;
    using digicard.Models.API.Request.Users;
    using digicard.Models.API.Request.Cards;
    using digicard.Models.API.Response.Cards;
    using digicard.Models.UI.View.Accounts;
    using digicard.Models.UI.Page;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.Page.Accounts;
    using digicard.Models.UI.Page.Cards;
    using digicard.Models.UI.View.Cards;

    [Authorize]
    public class CardsController : DigicardUIController
    {
        public CardsController(IWebHostEnvironment environment, IOptions<AppSettings> appSettings, IServiceProvider serviceProvider)
            : base(environment, appSettings, serviceProvider)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            CardsIndexPageModel model = new CardsIndexPageModel();
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> New()
        {
            CardsNewPageModel model = new CardsNewPageModel();
            
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> New(CardsNewPageModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    APIResult result = await CardsAPIContext.Current.Create(new CreateCardRequestModel()
                    {
                        NameOnCard = model.NewCard.NameOnCard,
                        Description = model.NewCard.Description,
                        Landline = model.NewCard.Landline,
                        Mobile = model.NewCard.Mobile,
                        Email = model.NewCard.Email,
                        Facebook = model.NewCard.Facebook,
                        Twitter = model.NewCard.Twitter,
                        Instagram = model.NewCard.Instagram,
                        Domain = model.NewCard.Domain,
                        AddressLine1 = model.NewCard.AddressLine1,
                        AddressLine2 = model.NewCard.AddressLine2,
                        AddressLine3 = model.NewCard.AddressLine3,
                        PostCode = model.NewCard.PostCode,
                        Country = model.NewCard.Country
                    });

                    if (result.Success)
                    {
                        // get objects
                        CardResponseModel response = JsonConvert.DeserializeObject<CardResponseModel>(Convert.ToString(result.Data));
                        model.NewCard = CardsMapper.MapCardResponseModelToCardViewModel(response);

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        model.ErrorMessages.AddRange(result.Errors.Select(e => e.FriendlyMessage));
                    }
                }
                catch (Exception ex)
                {
                    await this.Log.WriteSystemLog(ex);
                    model.ErrorMessages.Add(ex.Message);
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            CardsEditPageModel model = new CardsEditPageModel();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CardsEditPageModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    APIResult result = await CardsAPIContext.Current.Create(new CreateCardRequestModel()
                    {
                        // TODO: Add Fields
                        // BusinessName = model.NewCard.BusinessName,
                    });

                    if (result.Success)
                    {
                        // get objects
                        CardResponseModel response = JsonConvert.DeserializeObject<CardResponseModel>(Convert.ToString(result.Data));
                        model.Card = CardsMapper.MapCardResponseModelToCardViewModel(response);

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        model.ErrorMessages.AddRange(result.Errors.Select(e => e.FriendlyMessage));
                    }
                }
                catch (Exception ex)
                {
                    await this.Log.WriteSystemLog(ex);
                    model.ErrorMessages.Add(ex.Message);
                }
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Details(string id)
        {
            CardsDetailPageModel model = new CardsDetailPageModel();

            // TODO: if published or it's your card
            // TODO: IF not logged in show full page version
            model.Card = new CardViewModel()
            {
                NameOnCard = "Card 1"
            };

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
//        [Route("/cards/magicrouter/{key}")]
        public async Task<IActionResult> MagicRouter(string id)
        {
            CardsDetailPageModel model = new CardsDetailPageModel();

            APIResult result = await CardsAPIContext.Current.GetBySlug(id.ToLower());

            if(result.Success)
            {
                CardResponseModel response = JsonConvert.DeserializeObject<CardResponseModel>(Convert.ToString(result.Data));
                model.Card = CardsMapper.MapCardResponseModelToCardViewModel(response);
            }

            return View("Details", model);
        }
    }
}
