﻿namespace digicard.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using digicard.Components;
    using digicard.Components.API;
    using digicard.Components.API.Cards;
    using digicard.Components.Shared.API;
    using digicard.Components.UI.Users;
    using digicard.Components.UI.Authentication;
    using digicard.Mappers;
    using digicard.Models.API.Request.Users;
    using digicard.Models.API.Response.Accounts;
    using digicard.Models.API.Response.Cards;
    using digicard.Components.UI;
    using digicard.Components.UI.Cards;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.Page.Content;
//    using digicard.Models.UI.View.Content;

    [AllowAnonymous]
    public class ContentController : DigicardUIController
    {
        public ContentController(IWebHostEnvironment environment, IOptions<AppSettings> appSettings, IServiceProvider serviceProvider)
            : base(environment, appSettings, serviceProvider)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ContentIndexPageModel model = new ContentIndexPageModel();

            APIResult result = await CardsAPIContext.Current.GetRandomN(3);

            if(result.Success)
            {
                List<CardResponseModel> response = JsonConvert.DeserializeObject<List<CardResponseModel>>(Convert.ToString(result.Data));
                model.Examples = CardsMapper.MapCardResponseModelListToCardViewModelList(response);
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Contact()
        {
            ContentContactPageModel model = new ContentContactPageModel();
            return View(model);
        }
    }
}
