﻿using System.Net;
namespace digicard.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using digicard.Components;
    using digicard.Components.API;
    using digicard.Components.API.Authentication;
    using digicard.Components.UI;
    using digicard.Components.UI.Authentication;
    using digicard.Components.Shared.API;
    using digicard.Models.API.Request.Accounts;
    using digicard.Models.API.Request.Users;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.View.Users;

    public class AuthenticationController : DigicardUIController
    {
        public AuthenticationController(IWebHostEnvironment environment, IOptions<AppSettings> appSettings, IServiceProvider serviceProvider)
            : base(environment, appSettings, serviceProvider)
        {
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return await RedirectToHomepage();
            }

            LoginPageModel model = new LoginPageModel();

            if (HttpContext.Request.Query.Any(q => q.Key.ToLower() == "returnurl"))
            {
                model.ReturnUrl = HttpContext.Request.Query.First(q => q.Key.ToLower() == "returnurl").Value;
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginPageModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                return await RedirectToHomepage(returnUrl: model.ReturnUrl);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    UserViewModel user = await AuthenticationManager.Current.SignIn(model.EmailAddress, model.Password, Request.HttpContext.Connection.RemoteIpAddress.ToString());

                    if (user != null && user.IsAuthenticated)
                    {
                        return await RedirectToHomepage(returnUrl: model.ReturnUrl, activeUser: user);
                    }

                    model.ErrorMessages.Add("Email or Password incorrect.");
                }
                catch (Exception ex)
                {
                    await this.Log.WriteSystemLog(ex);
                    model.ErrorMessages.Add(ex.Message);
                }
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await AuthenticationManager.Current.SignOut();
            }

            return this.Redirect("/Authentication/Login");
        }

        [HttpGet]
        [Route("/register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                return await RedirectToHomepage();
            }

            RegisterPageModel model = new RegisterPageModel();

            if (HttpContext.Request.Query.Any(q => q.Key.ToLower() == "returnurl"))
            {
                model.ReturnUrl = HttpContext.Request.Query.First(q => q.Key.ToLower() == "returnurl").Value;
            }

            return View(model);
        }

        [HttpPost]
        [Route("/register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterPageModel model)
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return await this.RedirectToHomepage(returnUrl: model.ReturnUrl);
            }

            if (this.ModelState.IsValid)
            {
                try
                {
                    UserViewModel user = await AuthenticationManager.Current.Register(model.EmailAddress, model.Password);

                    if (user != null)
                    {
                        user = await AuthenticationManager.Current.SignIn(model.EmailAddress, model.Password, Request.HttpContext.Connection.RemoteIpAddress.ToString());

                        if (user != null && user.IsAuthenticated)
                        {
                            return await this.RedirectToHomepage(model.ReturnUrl);
                        }
                    }
                    else
                    {
                        model.ErrorMessages.Add("There was an error creating your account.");
                    }
                }
                catch (Exception ex)
                {
                    await this.Log.WriteSystemLog(ex);
                    model.ErrorMessages.Add(ex.Message);
                }
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> StartPasswordReset()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return await this.RedirectToHomepage();
            }

            StartPasswordResetPageModel model = new StartPasswordResetPageModel();

            return this.View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> StartPasswordReset(StartPasswordResetPageModel model)
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return await this.RedirectToHomepage();
            }

            if (this.ModelState.IsValid)
            {
                try
                {
                    APIResult result = await AuthenticationManager.Current.StartPasswordReset(model.EmailAddress);

                    if (result.Errors.Any())
                    {
                        model.ErrorMessages.AddRange(result.Errors.Select(t => t.FriendlyMessage).ToList());
                    }
                    else
                    {
                        model.AlertMessages.Add("A password reset email was sent.");
                    }
                }
                catch (Exception ex)
                {
                    await this.Log.WriteSystemLog(ex);
                    model.ErrorMessages.Add($"Failed to reset your password. Sorry! Please let us know and quote this error: {ex.Message}.");
                }
            }

            return this.View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult FinishPasswordReset(string id)
        {
            FinishPasswordResetPageModel model = new FinishPasswordResetPageModel
            {
                Token = id
            };

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> FinishPasswordReset(FinishPasswordResetPageModel model)
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return await this.RedirectToHomepage();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    FinishPasswordResetRequestModel requestModel = new FinishPasswordResetRequestModel()
                    {
                        Password = model.Password
                    };

                    APIResult result = await AuthenticationManager.Current.FinishPasswordReset(model.Token, requestModel);

                    if (result.Success)
                    {
                        model.ResetFinished = true;
                    }
                    else
                    {
                        model.ErrorMessages.AddRange(result.Errors.Select(e => e.FriendlyMessage));
                    }
                }
                catch (Exception ex)
                {
                    await this.Log.WriteSystemLog(ex);
                    model.ErrorMessages.Add($"Failed to reset your password. Sorry! Please let us know and quote this error: {ex.Message}.");
                }
            }

            return View(model);
        }

        private async Task<IActionResult> RedirectToHomepage(string returnUrl = null, UserViewModel activeUser = null)
        {
            UserViewModel current = activeUser == null ? await AuthenticationManager.Current.GetCurrentUser() : activeUser;

            if (current != null && current.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return this.Redirect(returnUrl);
                }

                if (this.HttpContext.Request.Query.Any(q => q.Key.ToLower() == "returnurl"))
                {
                    return this.Redirect(this.HttpContext.Request.Query.First(q => q.Key.ToLower() == "returnurl").Value);
                }

                // if (current.AvailableAccounts.Any())
                // {
                //     return this.Redirect("/Accounts/Dashboard");
                // }
                return this.Redirect("/Cards/Index");
            }

            AuthenticationManager.Current.SignOut();
            return this.NotFound();
        }
    }
}
