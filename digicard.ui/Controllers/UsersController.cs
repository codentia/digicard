﻿using System.Net;
namespace digicard.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using digicard.Components;
    using digicard.Components.API;
    using digicard.Components.Shared.API;
    using digicard.Components.UI.Users;
    using digicard.Components.UI.Authentication;
    using digicard.Mappers;
    using digicard.Models.API.Request.Users;
    using digicard.Models.API.Response.Accounts;
    using digicard.Components.UI;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.Page.Users;
    using digicard.Models.UI.View.Users;

    [Authorize]
    public class UsersController : DigicardUIController
    {
        public UsersController(IWebHostEnvironment environment, IOptions<AppSettings> appSettings, IServiceProvider serviceProvider)
            : base(environment, appSettings, serviceProvider)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Profile()
        {
            UsersProfilePageModel model = new UsersProfilePageModel();

            // try
            // {
            //     APIResult result = await AccountsAPIContext.Current.GetUsers(model.CurrentAccount.AccountId.ToString());

            //     if (result.Success)
            //     {
            //         List<LinkedUserResponseModel> users = JsonConvert.DeserializeObject<List<LinkedUserResponseModel>>(Convert.ToString(result.Data));
            //         model.Users = UsersMapper.MapLinkedUserResponseModelToLinkedUserViewModel(users);
            //     }
            //     else
            //     {
            //         model.ErrorMessages.AddRange(result.Errors.Select(e => e.FriendlyMessage));
            //     }
            // }
            // catch (Exception ex)
            // {
            //     await this.Log.WriteSystemLog(ex);
            //     model.ErrorMessages.Add(ex.Message);
            // }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Profile(UsersProfilePageModel model)
        {
            UserViewModel user = await AuthenticationManager.Current.GetCurrentUser();
            string id = user.UserId.ToString();

            if (ModelState.IsValid)
            {
                try
                {
                    APIResult result = await UsersAPIContext.Current.Update(id, new UpdateUserRequestModel
                    {
                        Password = model.Profile.Password,
                        ConfirmPassword = model.Profile.ConfirmPassword,
                    });

                    if (result.Success)
                    {
                        model.AlertMessages.Add("Profile updated successfully.");
                    }
                    else
                    {
                        model.ErrorMessages.AddRange(result.Errors.Select(e => e.FriendlyMessage));
                    }
                }
                catch (Exception ex)
                {
                    await this.Log.WriteSystemLog(ex);
                    model.ErrorMessages.Add(ex.Message);
                }
            }

            return View(model);
        }
    }
}
