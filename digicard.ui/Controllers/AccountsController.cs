﻿namespace digicard.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using digicard.Components;
    using digicard.Components.API;
    using digicard.Components.Shared.API;
    using digicard.Components.UI;
    using digicard.Components.UI.Accounts;
    using digicard.Components.UI.Users;
    using digicard.Components.UI.Authentication;
    using digicard.Mappers;
    using digicard.Models.API.Request.Users;
    using digicard.Models.API.Request.Accounts;
    using digicard.Models.API.Response.Accounts;
    using digicard.Models.UI.View.Accounts;
    using digicard.Models.UI.Page;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.Page.Accounts;

    [Authorize]
    public class AccountsController : DigicardUIController
    {
        public AccountsController(IWebHostEnvironment environment, IOptions<AppSettings> appSettings, IServiceProvider serviceProvider)
            : base(environment, appSettings, serviceProvider)
        {
        }

        [HttpGet]
        public async Task<IActionResult> New()
        {
            AccountsNewPageModel model = new AccountsNewPageModel();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> New(AccountsNewPageModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    APIResult result = await AccountsAPIContext.Current.Create(new CreateAccountRequestModel()
                    {
                        Name = model.NewAccount.Name
                    });

                    if (result.Success)
                    {
                        // get objects
                        AccountResponseModel response = JsonConvert.DeserializeObject<AccountResponseModel>(Convert.ToString(result.Data));
                        model.NewAccount = AccountsMapper.MapAccountResponseModelToAccountViewModel(response);

                        return RedirectToAction("Dashboard", "Accounts");
                    }
                    else
                    {
                        model.ErrorMessages.AddRange(result.Errors.Select(e => e.FriendlyMessage));
                    }
                }
                catch (Exception ex)
                {
                    await this.Log.WriteSystemLog(ex);
                    model.ErrorMessages.Add(ex.Message);
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Edit()
        {
            AccountsEditPageModel model = new AccountsEditPageModel();
            model.Account = model.CurrentAccount;

            try
            {
                APIResult result = await AccountsAPIContext.Current.GetUsers(model.CurrentAccount.AccountId.ToString());

                if (result.Success)
                {
                    // List<LinkedUserResponseModel> users = JsonConvert.DeserializeObject<List<LinkedUserResponseModel>>(Convert.ToString(result.Data));
                    // model.Users = UsersMapper.MapLinkedUserResponseModelToLinkedUserViewModel(users);

                    // // fetch invitations
                    // result = await AccountsAPIContext.Current.GetPendingInvitations(model.Account.AccountId.ToString());

                    // if (result.Success)
                    // {
                    //     List<InvitationResponseModel> invitations = JsonConvert.DeserializeObject<List<InvitationResponseModel>>(Convert.ToString(result.Data));
                    //     model.Invitations = AccountsMapper.MapInvitationResponseModelToInvitationViewModel(invitations);
                    // }
                    // else
                    // {
                    //     model.ErrorMessages.AddRange(result.Errors.Select(e => e.FriendlyMessage));
                    // }
                }
                else
                {
                    model.ErrorMessages.AddRange(result.Errors.Select(e => e.FriendlyMessage));
                }
            }
            catch (Exception ex)
            {
                await this.Log.WriteSystemLog(ex);
                model.ErrorMessages.Add(ex.Message);
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string id, AccountsEditPageModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    APIResult result = await AccountsAPIContext.Current.Update(id, new UpdateAccountRequestModel
                    {
                    });

                    if (result.Success)
                    {
                        model.AlertMessages.Add("Account updated successfully.");
                    }
                    else
                    {
                        model.ErrorMessages.AddRange(result.Errors.Select(e => e.FriendlyMessage));
                    }
                }
                catch (Exception ex)
                {
                    await this.Log.WriteSystemLog(ex);
                    model.ErrorMessages.Add(ex.Message);
                }
            }

            return View(model);
        }
    }
}
