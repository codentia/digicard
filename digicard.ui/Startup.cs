using System;
using System.IO.Pipes;
namespace digicard.ui
{
    using digicard;
    using digicard.Components.UI.Accounts;
    using digicard.Components.UI.Authentication;
    using digicard.Components.UI.Cards;
    using digicard.Components.UI.Subscriptions;
    using digicard.Components.UI.Users;
    using digicard.Models.UI.View.Users;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.DataProtection;
    using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.HttpOverrides;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Rewrite;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Net.Http.Headers;
    using Newtonsoft.Json.Serialization;
    using StackExchange.Redis;

    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: false)
            .AddJsonFile($"secrets/appsettings.{env.EnvironmentName}.json", true, false)
            .AddEnvironmentVariables();

            // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<AppSettings>();
            }

            this.Configuration = builder.Build();
            this.Environment = env;
        }
        
        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
       public void ConfigureServices(IServiceCollection services)
        {
            AppSettings.Current = Configuration.GetSection("AppSettings").Get<AppSettings>();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            services.AddOptions();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();

            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation();

            // services.AddCors(c =>
            // {
            //     c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            // });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(
                    "IsLoggedIn",
                    policy =>
                    {
                        policy.RequireAuthenticatedUser();
                    });
            });

            // services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddHttpContextAccessor();

            services.AddMvc().AddNewtonsoftJson(options =>
            {
                // Use the default property (Pascal) casing
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                options.SerializerSettings.TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Auto;
            });
            
//                services.AddWebOptimizer(minifyJavaScript:false, minifyCss:false);
            services.AddWebOptimizer(pipeline =>
                {
                    pipeline.AddCssBundle("/css/core.css", "assets/css/*.css").UseContentRoot();
                    pipeline.AddJavaScriptBundle("/js/core.js", "assets/js/*.js").UseContentRoot();
                }
            );

            // Identity Services
            services.AddTransient<UserManager<UserViewModel>>();
            services.AddTransient<ISessionsAPIContext, SessionsAPIContext>();
            services.AddSingleton<IUserStore<UserViewModel>, UserStore>();
            services.AddTransient<IAccountsAPIContext, AccountsAPIContext>();
            services.AddTransient<IUsersAPIContext, UsersAPIContext>();
            services.AddTransient<ICardsAPIContext, CardsAPIContext>();
            services.AddTransient<ISubscriptionsAPIContext, SubscriptionsAPIContext>();
            services.AddSingleton<IRoleStore<RoleViewModel>, UserRoleStore>();
            services.AddScoped<IUserClaimsPrincipalFactory<UserViewModel>, DigicardClaimsPrincipalFactory>();
            services.AddScoped<IAuthenticationManager, AuthenticationManager>();

            services.AddIdentity<UserViewModel, RoleViewModel>(options =>
            {
            })
            .AddDefaultTokenProviders()
            .AddClaimsPrincipalFactory<DigicardClaimsPrincipalFactory>();

            // If you want to tweak Identity cookies, they're no longer part of IdentityOptions.
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Authentication/Login";
                options.LogoutPath = "/Authentication/Logout";
                options.ExpireTimeSpan = System.TimeSpan.FromMinutes(30);
                options.SlidingExpiration = true;
            });

            AuthenticatedEncryptorConfiguration configuration = new AuthenticatedEncryptorConfiguration()
            {
                EncryptionAlgorithm = Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.EncryptionAlgorithm.AES_256_CBC,
                ValidationAlgorithm = Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ValidationAlgorithm.HMACSHA512
            };

            services.AddDataProtection()
                .PersistKeysToStackExchangeRedis(ConnectionMultiplexer.Connect(AppSettings.Current.RedisConnectionString), AppSettings.Current.RedisKey)
                .UseCryptographicAlgorithms(configuration: configuration);

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = AppSettings.Current.RedisConnectionString;
            });

            // services.AddTransient<IAccountsDbContext, AccountsDbContext>();
            // services.AddTransient<IMessageDbContext, MessageDbContext>();
            // services.AddTransient<IMessageContext, MessageContext>();
            // services.AddTransient<ILogDbContext, LogDbContext>();
            // services.AddTransient<IJobsDbContext, JobsDbContext>();
            // services.AddTransient<ILogMqContext, LogMqContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            AppSettings.Current = Configuration.GetSection("AppSettings").Get<AppSettings>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseHttpsRedirection();
    
            app.UseWebOptimizer();
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 60 * 60 * 24;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] =
                        "public,max-age=" + durationInSeconds;
                }
            });


            app.Use(async (context, next) =>
            {
                if(context.Request.Host.Host != AppSettings.Current.BaseDomain)
                {
                    if(string.IsNullOrEmpty(context.Request.Path.Value) || context.Request.Path.Value == "/")
                    {
                        context.Request.Path = $"/Cards/MagicRouter"; //?key={context.Request.Host}";
                        context.Request.QueryString = new Microsoft.AspNetCore.Http.QueryString($"?id={context.Request.Host.Host}");
                        //context.Request.RouteValues["controller"] = "cards";
                        //context.Request.RouteValues["action"] = "magicrouter";
//                        return;
                    }
                }

                await next();
            });

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                endpoints.MapControllerRoute(
                    name: "domain-default",
                    pattern: "{controller=Content}/{action=Index}/{id?}");
            });
        }
    }
}
