﻿namespace digicard.Mappers
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using digicard.Models.API.Response;
    using digicard.Components.Shared.API;

    public class APIErrorConverter : JsonConverter
    {
        public override bool CanWrite
        {
            get
            {
                return false;
            }
        }

        public override bool CanRead
        {
            get
            {
                return true;
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(IAPIError);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new InvalidOperationException("Use default serialization.");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonArray = JArray.Load(reader);
            List<IAPIError> items = new List<IAPIError>();

            foreach (var item in jsonArray)
            {
                var jsonObject = item as JObject;
                var instance = new APIError();

                // Populate the form field instance.
                serializer.Populate(jsonObject.CreateReader(), instance);
                items.Add(instance);
            }

            return items;
        }
    }
}
