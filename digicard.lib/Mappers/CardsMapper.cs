﻿namespace digicard.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using digicard.Components.API.Cards;
    using digicard.Models.API.Response.Cards; 
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.View.Cards;
    using digicard.Models.UI.View.Users;

    public sealed class CardsMapper
    {

        public static List<CardResponseModel> MapCardListToCardResponseModelList(List<Card> cards)
        {
            return cards.Select(MapCardToCardResponseModel).ToList();
        }

        public static CardResponseModel MapCardToCardResponseModel(Card card)
        {
            return new CardResponseModel
            {
                CardId = Convert.ToString(card.CardId),
                UserId = Convert.ToString(card.UserId),
                NameOnCard = card.NameOnCard,
                Description = card.Description,
                Landline = Convert.ToString(card.Landline),
                Mobile = Convert.ToString(card.Mobile),
                Email = card.Email,
                Facebook = card.Facebook,
                Twitter = card.Twitter,
                Instagram = card.Instagram,
                Domain = card.Domain,
                AddressLine1 = card.AddressLine1,
                AddressLine2 = card.AddressLine2,
                AddressLine3 = card.AddressLine3,
                PostCode = card.PostCode,
                Country = card.Country,
                IsPublished = Convert.ToString(card.IsPublished)
            };
        }

        public static CardViewModel MapCardResponseModelToCardViewModel(CardResponseModel responseModel)
        {
            return new CardViewModel
            {
                CardId = Convert.ToInt32(responseModel.CardId),
                UserId = Convert.ToInt32(responseModel.UserId),
                NameOnCard = responseModel.NameOnCard,
                Description = responseModel.Description,
                Landline = responseModel.Landline,
                Mobile = responseModel.Mobile,
                Email = responseModel.Email,
                Facebook = responseModel.Facebook,
                Twitter = responseModel.Twitter,
                Instagram = responseModel.Instagram,
                Domain = responseModel.Domain,
                AddressLine1 = responseModel.AddressLine1,
                AddressLine2 = responseModel.AddressLine2,
                AddressLine3 = responseModel.AddressLine3,
                PostCode = responseModel.PostCode,
                Country = responseModel.Country,
                IsPublished = Convert.ToBoolean(responseModel.IsPublished)
            };
        }

        public static List<CardViewModel> MapCardResponseModelListToCardViewModelList(List<CardResponseModel> responseModels)
        {
            return responseModels.Select(MapCardResponseModelToCardViewModel).ToList();
        }
    }
}
