﻿using System.Runtime.CompilerServices;
using System;
namespace digicard.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using digicard.Components.API.Accounts;
    using digicard.Models.API.Response.Accounts; 
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.View.Accounts;
    using digicard.Models.UI.View.Users;

    public sealed class AccountsMapper
    {
        public static AccountResponseModel MapAccountToAccountResponseModel(Account account)
        {
            return new AccountResponseModel
            {
                AccountId = account.AccountId.ToString(),
                UserId = account.UserId.ToString(),
                Name = account.Name,
                BrainTreeSubId = account.BrainTreeSubId,
                BrainTreeCustomerId = account.BrainTreeCustomerId,
                SubscriptionExpiry = account.SubscriptionExpiry.ToString(FormatString.API_DATE_TIME),
                SubscriptionCancelledAt = account.SubscriptionCancelledAt.HasValue ? account.SubscriptionCancelledAt.Value.ToString(FormatString.API_DATE_TIME) : String.Empty
            };
        }
        public static AccountViewModel MapAccountResponseModelToAccountViewModel(AccountResponseModel responseModel)
        {
            return new AccountViewModel
            {
                AccountId = Convert.ToInt32(responseModel.AccountId),
                UserId = Convert.ToInt32(responseModel.UserId),
                Name = responseModel.Name,
                BrainTreeSubId = responseModel.BrainTreeSubId,
                BrainTreeCustomerId = responseModel.BrainTreeCustomerId,
                SubscriptionExpiry = Convert.ToDateTime(responseModel.SubscriptionExpiry),
                SubscriptionCancelledAt = Convert.ToDateTime(responseModel.SubscriptionCancelledAt)
            };
        }
    }
}
