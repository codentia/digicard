﻿namespace digicard.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using digicard.Components.API.Users;
    using digicard.Models.API.Response.Users;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.View.Users;

    public sealed class UsersMapper
    {
        public static ILoginResponse MapLoginDataToILoginResponse(User user)
        {
            return new LoginResponseModel
            {
                User = user == null ? null : MapUserToIUserResponse(user),
            };
        }

        public static IUserResponse MapUserToIUserResponse(User user)
        {
            return new UserResponseModel
            {
                UserId = user.UserId.ToString(),
                EmailAddress = user.EmailAddress,
                IsAuthenticated = user.IsAuthenticated.ToString(),
                IsEmailValidated = user.IsEmailValidated.ToString(),
                SecurityToken = user.SecurityToken,
                SecurityTokenExpires = user.SecurityTokenExpires.ToString(FormatString.API_DATE_TIME)
            };
        }

        public static List<IUserResponse> MapUserToIUserResponse(List<User> users)
        {
            return users.Select(MapUserToIUserResponse).ToList();
        }

        public static UserViewModel MapUserResponseModelToUserViewModel(IUserResponse responseModel)
        {
            return new UserViewModel
            {
                UserId = Convert.ToInt32(responseModel.UserId),
                EmailAddress = responseModel.EmailAddress,
                SecurityToken = responseModel.SecurityToken,
                SecurityTokenExpires = Convert.ToDateTime(responseModel.SecurityTokenExpires),
                IsAuthenticated = Convert.ToBoolean(responseModel.IsAuthenticated),
                IsEmailValidated = Convert.ToBoolean(responseModel.IsEmailValidated)
            };
        }

        public static List<UserViewModel> MapUserResponseModelToUserViewModel(IList<IUserResponse> responseModels)
        {
            return responseModels.Select(MapUserResponseModelToUserViewModel).ToList();
        }
    }
}
