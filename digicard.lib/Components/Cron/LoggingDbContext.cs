﻿namespace digicard.Components.Cron
{
    using System;
    using System.Data;
    using System.Threading.Tasks;
    using Codentia.Common.Data;
    using digicard.Components.Shared.Logging;

    public class LoggingDbContext : DatabaseContext, ILoggingDbContext
    {
        private const string WriteSystemLogQuery = "INSERT INTO systemlog (AccountId, UserId, Component, Method, Severity, Detail, StackTrace, LoggedAt) VALUES (NULLIF(@AccountId, 0), NULLIF(@UserId, 0), @Component, @Method, @Severity, @Detail, @StackTrace, @LoggedAt);";
        private const string WriteAuditLogQuery = "INSERT INTO auditlog (AccountId, UserId, ObjectId, ObjectType, OperationType, Detail, LogStamp) VALUES (@AccountId, @UserId, @ObjectId, @ObjectType, @OperationType, @Detail, @LogStamp);";

        public static ILoggingDbContext Current { get; set; }

        public async Task WriteSystemLog(LogEntry logEntry)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@AccountId", DbType.Int32, logEntry.AccountId),
                new DbParameter("@UserId", DbType.Int32, logEntry.UserId),
                new DbParameter("@Component", DbType.String, 150, logEntry.Component),
                new DbParameter("@Method", DbType.String, 150, logEntry.Method),
                new DbParameter("@Severity", DbType.Int32, logEntry.Severity),
                new DbParameter("@Detail", DbType.String, -1, logEntry.Detail),
                new DbParameter("@StackTrace", DbType.String, -1, logEntry.Trace ?? string.Empty),
                new DbParameter("@LoggedAt", DbType.DateTime, logEntry.LogStamp),
            };

            await this.Execute<DBNull>(WriteSystemLogQuery, parameters, commandTimeout: 120);
        }

        public async Task WriteAuditLog(AuditLogEntry auditLogEntry)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@AccountId", DbType.Int32, auditLogEntry.AccountId),
                new DbParameter("@UserId", DbType.Int32, auditLogEntry.UserId),
                new DbParameter("@ObjectId", DbType.Int32, auditLogEntry.ObjectId),
                new DbParameter("@ObjectType", DbType.String, 50, auditLogEntry.ObjectType.ToString()),
                new DbParameter("@OperationType", DbType.String, 10, auditLogEntry.OperationType.ToString()),
                new DbParameter("@Detail", DbType.String, -1, auditLogEntry.Detail),
                new DbParameter("@LogStamp", DbType.DateTime, auditLogEntry.LogStamp),
            };

            await this.Execute<DBNull>(WriteAuditLogQuery, parameters, commandTimeout: 120);
        }
    }
}
