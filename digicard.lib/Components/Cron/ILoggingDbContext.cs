﻿namespace digicard.Components.Cron
{
    using System.Data;
    using System.Threading.Tasks;
    using digicard.Components.Shared.Logging;

    public interface ILoggingDbContext
    {
        Task WriteSystemLog(LogEntry logEntry);

        Task WriteAuditLog(AuditLogEntry auditLogEntry);
    }
}
