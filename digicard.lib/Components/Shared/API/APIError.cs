﻿namespace digicard.Components.Shared.API
{
    using System.Collections.Generic;
    using digicard.Models.API.Response;
    using digicard.Models.API.Request;

    public class APIError : IAPIError
    {
        public APIError()
        {
        }

        public APIError(string code, string friendlyMessage, string diagnostic)
        {
            this.Code = code;
            this.FriendlyMessage = friendlyMessage;
            this.Diagnostic = diagnostic;
            this.IsInternalError = true;
        }

        public APIError(string code, string friendlyMessage, string diagnostic, bool isInternalError = true)
        {
            this.Code = code;
            this.FriendlyMessage = friendlyMessage;
            this.Diagnostic = diagnostic;
            this.IsInternalError = isInternalError;
        }

        public string Code { get; set; }

        public string Detail => !string.IsNullOrEmpty(FriendlyMessage) ? FriendlyMessage : Diagnostic;

        public string Diagnostic { get; set; }

        public string FriendlyMessage { get; set; }

        public bool IsInternalError { get; set; }

        public static APIError GetValidationAPIError(BaseResource baseResource, SubResource subResource, int errorId, Dictionary<int, string> errorDictionary, APIHttpMethod httpMethod, string errorText = "")
        {
            string code = BuildErrorCode(baseResource, subResource, httpMethod, errorId);
            string detail = string.Format(errorDictionary[errorId], errorText);
            return new APIError(code, detail, detail, false);
        }

        public static APIError GetNullRequestModelError(DigicardBaseAPIRequestModel requestModel)
        {
            string code = BuildErrorCode(requestModel.BaseResource, requestModel.SubResource, requestModel.HttpMethod, CommonErrors.MissingRequestModel);
            string detail = string.Format(CommonErrors.Instance[CommonErrors.MissingRequestModel], requestModel.Name);
            return new APIError(code, detail, detail, false);
        }

        public static APIError GetInternalAPIError(DigicardBaseAPIRequestModel requestModel, int errorId, Dictionary<int, string> errorDictionary, string errorText = "", bool overrideFriendlyMessage = false)
        {
            string code = BuildErrorCode(requestModel.BaseResource, requestModel.SubResource, requestModel.HttpMethod, errorId);
            string friendlyMessage = errorDictionary[errorId];
            if (overrideFriendlyMessage == false)
            {
                friendlyMessage = CommonErrors.UnexpectedExceptionFriendlyMessage;
            }
            else
            {
                friendlyMessage = string.Format(errorDictionary[errorId], errorText);
            }

            string diagnostic = string.Format(errorDictionary[errorId], errorText);

            string detail = string.Format(errorDictionary[errorId], errorText);
            return new APIError(code, detail, friendlyMessage, true);
        }

        public static APIError GetUnexpectedAPIError(DigicardBaseAPIRequestModel requestModel, string errorText = "")
        {
            return GetUnexpectedAPIError(requestModel.BaseResource, requestModel.SubResource, requestModel.HttpMethod, errorText);
        }

        public static APIError GetUnexpectedAPIError(BaseResource baseResource, SubResource subResource, APIHttpMethod httpMethod, string errorText = "")
        {
            int errorId = CommonErrors.UnexpectedException;
            //// string code = BuildErrorCode(requestModel.BaseResource, requestModel.SubResource, requestModel.HttpMethod, errorId);
            string code = BuildErrorCode(baseResource, subResource, httpMethod, errorId);
            string friendlyMessage = CommonErrors.UnexpectedExceptionFriendlyMessage;
            string diagnostic = string.Format(CommonErrors.Instance[errorId], errorText);
            return new APIError(code, friendlyMessage, diagnostic, true);
        }

        private static string BuildErrorCode(BaseResource baseResource, SubResource subResource, APIHttpMethod method, int errorId)
        {
            return string.Format("{0}/{1}/{2}/{3}", method.ToString().ToUpper(), baseResource, subResource, errorId);
        }
    }
}
