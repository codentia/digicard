﻿namespace digicard.Components.Shared.API
{
    public enum BaseResource
    {
        System = 1,

        Accounts = 2,

        Users = 3,

        Logging = 4,

        Cards = 5
    }
}
