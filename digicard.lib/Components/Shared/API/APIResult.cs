﻿namespace digicard.Components.Shared.API
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using digicard.Models.API.Response;
    using digicard.Mappers;

    public class APIResult : IAPIResult
    {
        public APIResult()
        {
            this.Errors = new List<IAPIError>();
        }

        public bool Success => Errors.Count == 0;

        public bool IsFromWebCache { get; set; }

        [JsonConverter(typeof(APIErrorConverter))]
        public List<IAPIError> Errors { get; set; }

        public dynamic Data { get; set; }

        public bool AuthenticationFailed { get; set; }
    }
}
