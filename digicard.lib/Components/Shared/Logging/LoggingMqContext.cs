﻿namespace digicard.Components.Shared.Logging
{
    using System.Threading.Tasks;
    using digicard.Components;

    public class LoggingMqContext : MessageQueueContext, ILoggingMqContext
    {
        public LoggingMqContext()
            : base("log")
        {
        }

        public static ILoggingMqContext Current { get; set; }

        public MessageQueueContext Queue => this;

        public Task WriteLogEntryToQueue(LogEntry item)
        {
            WriteToQueue(item, "log");

            return Task.CompletedTask;
        }

        public Task WriteAuditLogEntryToQueue(AuditLogEntry item)
        {
            WriteToQueue(item, "audit");

            return Task.CompletedTask;
        }
    }
}
