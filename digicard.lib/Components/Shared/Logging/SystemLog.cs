﻿namespace digicard.Components.Shared.Logging
{
    using System;
    using System.Threading.Tasks;

    public class SystemLog
    {
        public string Component { get; set; }

        public string Method { get; set; }

        public async Task WriteSystemLog(int severity, string detail, string trace = "")
        {
            // await LoggingAPIContext.Current.WriteSystemLog(new Models.API.Request.Logging.CreateSystemLogRequestModel
            // {
            //     Component = this.Component,
            //     Method = this.Method,
            //     Severity = severity.ToString(),
            //     Detail = detail,
            //     Trace = trace 
            // });

            throw new NotImplementedException();
        }

        public async Task WriteSystemLog(Exception ex)
        {
            while (ex != null)
            {
                //SentrySdk.CaptureException(ex);

                await WriteSystemLog(100, ex.Message, ex.StackTrace);
                ex = ex.InnerException;
            }
        }
    }
}
