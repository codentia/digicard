﻿namespace digicard.Components.Shared.Logging
{
    using System;
    using System.Threading.Tasks;

    public interface ILoggingMqContext
    {
        MessageQueueContext Queue { get; }

        Task WriteLogEntryToQueue(LogEntry item);

        Task WriteAuditLogEntryToQueue(AuditLogEntry item);
    }
}
