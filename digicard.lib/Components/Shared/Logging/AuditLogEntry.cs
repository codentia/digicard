﻿namespace digicard.Components.Shared.Logging
{
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API.Accounts;
    using digicard.Components.API.Users;
    using digicard.Components.UI.Users;
    using digicard.Components.UI.Authentication;
    using digicard.Common;

    public class AuditLogEntry : IMessageQueueItem
    {
        public AuditLogEntry()
        {
            this.Detail = string.Empty;
            this.LogStamp = DateTime.Now;
        }

        public static bool DumpToConsole { get; set; }

        public int AccountId { get; set; }

        public int UserId { get; set; }

        public int ObjectId { get; set; }

        public AuditObjectType ObjectType { get; set; }

        public AuditOperationType OperationType { get; set; }

        public string Detail { get; set; }

        public DateTime LogStamp { get; set; }

        public static async Task Write(Account account, User user, AuditOperationType operation, AuditObjectType objectType, int objectId = 0, string detail = "")
        {
            await new AuditLogEntry()
            {
                AccountId = account == null ? 0 : account.AccountId,
                UserId = user == null ? 0 : user.UserId,
                OperationType = operation,
                ObjectType = objectType,
                ObjectId = objectId,
                Detail = detail,
                LogStamp = DateTime.Now
            }.Write();
        }

        public async Task Write()
        {
            if (DumpToConsole)
            {
                Console.Out.WriteLine($"[{this.AccountId}/{this.UserId}]:{this.ObjectType}:{this.OperationType}:{this.ObjectId} - {this.Detail}");
            }

            await LoggingMqContext.Current.WriteAuditLogEntryToQueue(this);
        }

        public async Task Save()
        {
            //await LoggingDbContext.Current.WriteAuditLog(this);

            throw new NotImplementedException();
        }
    }
}
