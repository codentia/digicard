﻿namespace digicard.Components.Shared.Logging
{
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Common;
    using digicard.Components.API.Accounts;
    using digicard.Components.Shared.API;

    public class LogEntry : IMessageQueueItem
    {
        public LogEntry()
        {
            this.Trace = string.Empty;
            this.Severity = 1;
            this.LogStamp = DateTime.Now;
        }

        public static bool DumpToConsole { get; set; }

        public string Component { get; set; }

        public string Method { get; set; }

        public int Severity { get; set; }

        public string Detail { get; set; }

        public string Trace { get; set; }

        public int AccountId { get; set; }

        public int UserId { get; set; }

        public DateTime LogStamp { get; set; }

        public static async Task Write(string component, string method, Exception ex, int userId = 0, int accountId = 0)
        {
            while (ex != null)
            {
                await new LogEntry()
                {
                    Component = component,
                    Method = method,
                    Detail = ex.Message,
                    Severity = 100,
                    Trace = ex.StackTrace,
                    UserId = userId,
                    AccountId = accountId
                }.Write();

                //SentrySdk.CaptureException(ex);

                ex = ex.InnerException;
            }
        }

        public static async Task Write(string component, string method, string details, int severity = 0, int userId = 0, int accountId = 0)
        {
            await new LogEntry()
            {
                Component = component,
                Method = method,
                Detail = details,
                Severity = severity,
                UserId = userId,
                AccountId = accountId
            }.Write();
        }

        public static async Task Write(string component, string method, APIResult result, int userId = 0, int accountId = 0)
        {
            StringBuilder errorList = new StringBuilder();
            int severity = 0;

            foreach (APIError error in result.Errors)
            {
                errorList.Append($"{error.Code}: {error.Diagnostic}  ");
                severity = error.IsInternalError ? 100 : 1;
            }

            string trace = $"{method} result - {result.Success}";

            await new LogEntry()
            {
                Component = component,
                Method = method,
                Detail = errorList.ToString(),
                Trace = trace,
                Severity = severity,
                UserId = userId,
                AccountId = accountId
            }.Write();
        }

        public async Task Write()
        {
            if (DumpToConsole)
            {
                Console.Out.WriteLine($"[{this.Component}/{this.Method}]:{this.Severity}:{this.Detail}");

                if (!string.IsNullOrEmpty(this.Trace))
                {
                    Console.Out.WriteLine(this.Trace);
                }
            }

            await LoggingMqContext.Current.WriteLogEntryToQueue(this);
        }

        public async Task Save()
        {
            //await LoggingDbContext.Current.WriteSystemLog(this);
            throw new NotImplementedException();
        }
    }
}
