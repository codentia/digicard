﻿namespace digicard.Components.Shared
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Newtonsoft.Json;
    using RabbitMQ.Client;
    using RabbitMQ.Client.Events;
    using digicard.Common;

    public abstract class MessageQueueContext
    {
        private IConnection _processConnection;
        private IModel _processChannel;

        protected MessageQueueContext(string routingKey)
        {
            this.RoutingKey = string.Concat(AppSettings.Current.MessageQueueRoutingKey, ".", routingKey);

            this.Factory = new ConnectionFactory()
            {
                HostName = AppSettings.Current.MessageQueueHost,
                AutomaticRecoveryEnabled = true,
                RequestedHeartbeat = new TimeSpan(0, 0, 60),
                DispatchConsumersAsync = true,
            };

            if (!string.IsNullOrEmpty(AppSettings.Current.MessageQueueUsername))
            {
                this.Factory.UserName = AppSettings.Current.MessageQueueUsername;
                this.Factory.Password = AppSettings.Current.MessageQueuePassword;
            }
        }

        protected string RoutingKey { get; set; }

        protected string ConsumerKey { get; set; }

        protected ConnectionFactory Factory { get; set; }

        public void StartListeningToQueue(AsyncEventHandler<BasicDeliverEventArgs> pushHandler, ushort batchSize)
        {
            _processConnection = this.Factory.CreateConnection();
            _processChannel = _processConnection.CreateModel();

            _processChannel.QueueDeclare(queue: this.RoutingKey, durable: true, exclusive: false, autoDelete: false, arguments: null);

            _processChannel.BasicQos(0, batchSize, false);

            AsyncEventingBasicConsumer consumer = new AsyncEventingBasicConsumer(_processChannel);

            consumer.Received += pushHandler;

            // actually start the consumer
            this.ConsumerKey = _processChannel.BasicConsume(this.RoutingKey, false, consumer);
        }

        public void StopListeningToQueue()
        {
            if (_processChannel != null)
            {
                try
                {
                    _processChannel.BasicCancel(this.ConsumerKey);

                    if (!_processChannel.IsClosed)
                    {
                        _processChannel.Close();
                    }
                }
                catch (Exception ex)
                {
                    // mask exception - we are shutting down, so can't do a lot about it!
                    Console.Out.WriteLine(ex.Message);
                    Console.Out.WriteLine(ex.StackTrace);
                }
            }
        }

        public void AcknowledgeMessage(ulong deliveryTag)
        {
            if (_processChannel != null)
            {
                _processChannel.BasicAck(deliveryTag, false);
            }
        }

        protected void WriteToQueue(IMessageQueueItem item, string messageType)
        {
            using (IConnection connection = this.Factory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare("direct-exchange", ExchangeType.Direct, true, false, arguments: null);

                    channel.QueueDeclare(queue: this.RoutingKey, durable: true, exclusive: false, autoDelete: false, arguments: null);

                    channel.QueueBind(this.RoutingKey, "direct-exchange", this.RoutingKey, null);

                    string message = JsonConvert.SerializeObject(item);
                    var body = Encoding.UTF8.GetBytes(message);

                    string exchangeName = string.Empty;

                    var properties = channel.CreateBasicProperties();
                    properties.Headers = new Dictionary<string, object>();
                    properties.Headers.Add("message_type", messageType);
                    properties.Persistent = true;
                    properties.MessageId = Guid.NewGuid().ToString();

                    channel.BasicPublish(exchange: string.Empty, routingKey: this.RoutingKey, basicProperties: properties, body: body);
                }
            }
        }
    }
}
