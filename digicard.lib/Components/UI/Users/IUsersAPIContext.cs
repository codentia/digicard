﻿namespace digicard.Components.UI.Users
{
    using System.Threading.Tasks;
    using digicard.Components.API;
    using digicard.Components.Shared.API;    
    using digicard.Models.API.Request.Users;

    public interface IUsersAPIContext
    {
        Task<APIResult> Create(CreateUserRequestModel requestModel);

        Task<APIResult> Update(string id, UpdateUserRequestModel requestModel);

        Task<APIResult> Get(string id);

        Task<APIResult> StartPasswordReset(StartPasswordResetRequestModel requestModel);

        Task<APIResult> FinishPasswordReset(string token, FinishPasswordResetRequestModel requestModel);
    }
}
