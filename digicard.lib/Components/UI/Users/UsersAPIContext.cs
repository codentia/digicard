﻿namespace digicard.Components.UI.Users
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Options;
    using digicard;
    using digicard.Components.UI.API;
    using digicard.Components.Shared.API;
    using digicard.Models.API.Request.Users;

    public class UsersAPIContext : DigicardAPIContext, IUsersAPIContext
    {
        public UsersAPIContext(IOptions<AppSettings> appSettings, IHttpContextAccessor context)
            : base(appSettings, context)
        {
        }

        public static IUsersAPIContext Current { get; set; }

        public async Task<APIResult> Create(CreateUserRequestModel requestModel)
        {
            return await this.CallAPI(APIHttpMethod.Post, $"users", requestModel);
        }

        public async Task<APIResult> Update(string id, UpdateUserRequestModel requestModel)
        {
            return await this.CallAPI(APIHttpMethod.Put, $"users/{id}", requestModel);
        }
        
        public async Task<APIResult> Get(string id)
        {
            return await this.CallAPI(APIHttpMethod.Get, $"users/{id}", string.Empty);
        }

        public async Task<APIResult> StartPasswordReset(StartPasswordResetRequestModel requestModel)
        {
            return await this.CallAPI(APIHttpMethod.Post, $"users/reset", requestModel);
        }

        public async Task<APIResult> FinishPasswordReset(string token, FinishPasswordResetRequestModel requestModel)
        {
            return await this.CallAPI(APIHttpMethod.Put, $"accounts/reset/{token}", requestModel);
        }
    }
}
