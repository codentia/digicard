﻿namespace digicard.Components.UI
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using digicard.Components.Shared.Logging;
    using digicard.Components.UI.Accounts;
    using digicard.Components.UI.Authentication;
    using digicard.Components.UI.Users;
    using digicard.Models.UI.Page;
    using digicard.Models.UI.View;
    using digicard.Models.UI.View.Users;
    using digicard.Components.UI.Cards;
    using digicard.Components.UI.Subscriptions;


    public class DigicardUIController : Controller
    {
        private UserViewModel _user;

        public DigicardUIController(IWebHostEnvironment environment, IOptions<AppSettings> appSettings, IServiceProvider serviceProvider)
        {
            if (environment == null)
            {
                throw new ArgumentNullException(nameof(environment));
            }

            if (appSettings == null)
            {
                throw new ArgumentNullException(nameof(appSettings));
            }

            if (serviceProvider == null)
            {
                throw new ArgumentNullException(nameof(serviceProvider));
            }

            // store references
            AppSettings.Current = appSettings.Value;
            this.ServiceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
            this.Environment = environment.EnvironmentName;

            if (environment != null)
            {
                this.HostingEnvironment = environment;
            }

            // set up common contexts
            AuthenticationManager.Current = ServiceProvider.GetService<IAuthenticationManager>();

            // if (LoggingAPIContext.Current == null)
            // {
            //     LoggingAPIContext.Current = ServiceProvider.GetService<ILoggingAPIContext>();
            // }

            // if (SessionsAPIContext.Current == null)
            // {
            //     SessionsAPIContext.Current = ServiceProvider.GetService<ISessionsAPIContext>();
            // }

            if (UsersAPIContext.Current == null)
            {
                UsersAPIContext.Current = ServiceProvider.GetService<IUsersAPIContext>();
            }

            if (AccountsAPIContext.Current == null)
            {
                AccountsAPIContext.Current = ServiceProvider.GetService<IAccountsAPIContext>();
            }

            if (CardsAPIContext.Current == null)
            {
                CardsAPIContext.Current = ServiceProvider.GetService<ICardsAPIContext>();
            }

            if (SubscriptionsAPIContext.Current == null)
            {
                SubscriptionsAPIContext.Current = ServiceProvider.GetService<ISubscriptionsAPIContext>();
            }
        }

        public string Environment { get; set; }

        protected string Component => this.ControllerContext.RouteData.Values["area"] != null ? $"{this.ControllerContext.RouteData.Values["area"]}/{this.ControllerContext.RouteData.Values["controller"]}" : Convert.ToString(this.ControllerContext.RouteData.Values["controller"]);

        protected string Method
        {
            get
            {
                return Convert.ToString(this.ControllerContext.RouteData.Values["action"]);
            }
        }

        protected SystemLog Log => new SystemLog()
        {
            Component = this.Component,
            Method = this.Method
        };

        protected IServiceProvider ServiceProvider { get; set; }

        protected IWebHostEnvironment HostingEnvironment { get; set; }

        /*
        protected async Task<UserViewModel> GetCurrentUser()
        {
            if (_user == null)
            {
                if (User.Identity.IsAuthenticated)
                {
                    AuthenticationManager mgr = (AuthenticationManager)ServiceProvider.GetService<IAuthenticationManager>();
                    _user = await mgr.GetCurrentUser();
                }
            }

            return _user;
        }

        protected async Task<BasePageModel> PreparePageModel(BasePageModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                model.CurrentUser = await this.GetCurrentUser();
            }

            return model;
        }
        */
    }
}
