﻿namespace digicard.Components.UI.API
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Options;
    using digicard;

    public class DigicardAPIContext : BaseAPIContext
    {
        public DigicardAPIContext(IOptions<AppSettings> settings = null, IHttpContextAccessor context = null)
            : base(settings.Value.APIUrl, context)
        {
            if (settings != null)
            {
                AppSettings.Current = settings.Value;
            }
        }
    }
}
