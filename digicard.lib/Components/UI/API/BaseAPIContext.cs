﻿namespace digicard.Components.UI.API
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Net.Http.Json;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    using digicard.Components.Shared.API;
    using digicard.Components.UI.Authentication;
    using digicard.Models.UI.View.Users;

    public abstract class BaseAPIContext
    {
        private IHttpContextAccessor _context;

        public BaseAPIContext(string baseAddress, IHttpContextAccessor context = null)
        {
            BaseAddress = baseAddress;
            _context = context;
        }

        protected string Resource { get; set; }

        protected string Component { get; set; }

        protected string Method { get; set; }

        protected string BaseAddress { get; set; }

        protected virtual async Task<APIResult> CallAPI<TData>(APIHttpMethod method, string resource, TData data, bool isInternalCall = false)
        {
            APIResult result = new APIResult();
            resource = string.Concat(this.Resource, resource);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            using (HttpClient api = new HttpClient())
            {
                api.BaseAddress = new Uri(BaseAddress);
                api.DefaultRequestHeaders.Accept.Clear();
                api.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // get the user's token, if available
                string apiToken = string.Empty;

                if (_context != null)
                {
                    if (_context.HttpContext.User.Identity.IsAuthenticated)
                    {
                        if (_context.HttpContext.User.Claims.Any(c => c.Type == ClaimTypes.Thumbprint))
                        {
                            apiToken = _context.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.Thumbprint).Value;

                            if (!isInternalCall)
                            {
                                DateTime tokenExpiresAt = DateTime.FromBinary(Convert.ToInt64(_context.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.Expiration).Value));

                                /*
                                if (DateTime.Now.AddMinutes(5) > tokenExpiresAt)
                                {
                                    // less than 5 mins, refresh token
                                    APIResult refreshResult = await this.CallAPI<string>(ApiHttpMethod.Put, "sessions", string.Empty, isInternalCall: true);
                                    if (refreshResult.Success)
                                    {
                                        RefreshSessionResponseModel refresh = JsonConvert.DeserializeObject<RefreshSessionResponseModel>(Convert.ToString(refreshResult.Data));

                                        ((ClaimsIdentity)_context.HttpContext.User.Identity).AddClaim(new Claim(ClaimTypes.Thumbprint, refresh.SecurityToken));
                                        ((ClaimsIdentity)_context.HttpContext.User.Identity).AddClaim(new Claim(ClaimTypes.Expiration, refresh.SecurityTokenExpires));
                                        apiToken = refresh.SecurityToken;
                                    }
                                }*/
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(apiToken))
                {
                    api.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiToken);
                }

                HttpResponseMessage response = null;

                string parameters = JsonConvert.SerializeObject(data);

                try
                {
                    switch (method)
                    {
                        case APIHttpMethod.Post:
                            response = await api.PostAsJsonAsync<TData>(resource, data);
                            break;
                        case APIHttpMethod.Put:
                            response = await api.PutAsJsonAsync<TData>(resource, data);
                            break;
                        case APIHttpMethod.Get:
                            response = await api.GetAsync(resource);
                            break;
                        case APIHttpMethod.Delete:
                            response = await api.DeleteAsync(resource);
                            break;
                        case APIHttpMethod.Patch:
                            response = await api.PatchAsJsonAsync<TData>(resource, data);
                            break;
                    }

                    if (response != null)
                    {
                        string content = await response.Content.ReadAsStringAsync();

                        if (response.IsSuccessStatusCode)
                        {
                            try
                            {
                                /*
                                var formatter = new JsonMediaTypeFormatter
                                {
                                    SerializerSettings = { TypeNameHandling = TypeNameHandling.Auto }
                                };

                                result = await response.Content.ReadAsAsync<APIResult>(new List<MediaTypeFormatter> { formatter });
                                */
                                result = JsonConvert.DeserializeObject<APIResult>(content);
                            }
                            catch (Exception ex)
                            {
                                // SentrySdk.CaptureException(ex);
                                result.Errors.Add(new APIError("CallAPI", CommonErrors.UnexpectedExceptionFriendlyMessage, ex.Message));
                            }
                        }
                        else
                        {
                            result.Errors.Add(new APIError("CallAPI", CommonErrors.UnexpectedExceptionFriendlyMessage, $"response code: {response.StatusCode}"));
                        }
                    }
                }
                catch (Exception ex)
                {
                    // SentrySdk.CaptureException(ex);
                    result.Errors.Add(new APIError("CallAPI", CommonErrors.UnexpectedExceptionFriendlyMessage, ex.Message));
                }

                result.AuthenticationFailed = result.AuthenticationFailed || (response != null && response.StatusCode == HttpStatusCode.Unauthorized);
            }

            if (result.AuthenticationFailed)
            {
                result.Errors.Add(new APIError("CallAPI", CommonErrors.AuthenticationTokenHasExpiredMessage, string.Empty));

                if (_context != null)
                {
                    if (_context.HttpContext.User.Identity.IsAuthenticated)
                    {
                        await Components.UI.Authentication.AuthenticationManager.Current.SignOut();
                    }
                }
            }

            return result;
        }
    }
}
