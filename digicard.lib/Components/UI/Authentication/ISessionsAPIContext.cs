﻿namespace digicard.Components.UI.Authentication
{
    using System.Threading.Tasks;
    using digicard.Components.Shared.API;
    using digicard.Models.API.Request.Sessions;

    public interface ISessionsAPIContext
    {
        Task<APIResult> CreateSession(CreateSessionRequestModel request);

        Task<APIResult> RetrieveSession(string emailAddress, bool allowCache = false);

        Task<APIResult> EndSession(string emailAddress);
    }
}
