﻿namespace digicard.Components.UI.Authentication
{
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Options;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.View.Users;

    public class DigicardClaimsPrincipalFactory : UserClaimsPrincipalFactory<UserViewModel, RoleViewModel>
    {
        public DigicardClaimsPrincipalFactory(UserManager<UserViewModel> userManager, RoleManager<RoleViewModel> roleManager, IOptions<IdentityOptions> optionsAccessor)
            : base(userManager, roleManager, optionsAccessor)
        {
        }

        public async override Task<ClaimsPrincipal> CreateAsync(UserViewModel user)
        {
            var principal = await base.CreateAsync(user);

            ((ClaimsIdentity)principal.Identity).AddClaim(new Claim(ClaimTypes.Thumbprint, user.SecurityToken));
            ((ClaimsIdentity)principal.Identity).AddClaim(new Claim(ClaimTypes.Expiration, user.SecurityTokenExpires.ToBinary().ToString()));

            return principal;
        }
    }
}
