﻿namespace digicard.Components.UI.Authentication
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using digicard;
    using digicard.Components.Shared.API;
    using digicard.Components.Shared.Logging;
    using digicard.Components.UI.Authentication;
    using digicard.Components.UI.Users;
    using digicard.Mappers;
    using digicard.Models.API.Request.Users;
    using digicard.Models.API.Request.Sessions;
    using digicard.Models.API.Response.Users;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.View.Users;

    public class AuthenticationManager : IAuthenticationManager
    {
        private UserViewModel _cachedUser;

        public AuthenticationManager(IOptions<AppSettings> settings, UserManager<UserViewModel> userManager, SignInManager<UserViewModel> signInManager, IHttpContextAccessor httpContext)
        {
            ////IServiceProvider serviceProvider
            AppSettings.Current = settings.Value;
            this.UserManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            this.SignInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            this.Context = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
        }

        public static IAuthenticationManager Current { get; set; }

        public IServiceProvider ServiceProvider { get; set; }

        public UserManager<UserViewModel> UserManager { get; set; }

        public SignInManager<UserViewModel> SignInManager { get; private set; }

        public IHttpContextAccessor Context { get; private set; }

        public async Task<UserViewModel> GetCurrentUser()
        {            
            System.Security.Principal.IIdentity identity = this.Context.HttpContext.User.Identity;

            if (identity.IsAuthenticated)
            {
                if(_cachedUser == null)
                {
                    _cachedUser = await this.UserManager.FindByIdAsync(identity.Name);
                }

                if (_cachedUser == null)
                {
                    await this.SignInManager.SignOutAsync();
                }
            }

            return _cachedUser;
        }

        public async Task<UserViewModel> SignIn(string username, string password, string ipAddress)
        {
            UserViewModel user = null;

            CreateSessionRequestModel requestModel = new CreateSessionRequestModel()
            {
                Username = username,
                Password = password,
                IPAddress = ipAddress
            };

            APIResult result = await SessionsAPIContext.Current.CreateSession(requestModel);

            if (result.Success)
            {
                if (result.Data != null)
                {
                    LoginResponseModel model = JsonConvert.DeserializeObject<LoginResponseModel>(Convert.ToString(result.Data));

                    if (model != null)
                    {
                        if (model.User != null)
                        {
                            user = UsersMapper.MapUserResponseModelToUserViewModel(model.User as UserResponseModel);

                            if (user.IsAuthenticated)
                            {
                                await this.SignInManager.SignInAsync(user, false);
                            }
                        }
                        else
                        {
                            user = new UserViewModel { EmailAddress = username, IsAuthenticated = false };
                        }
                    }
                }
            }

            return user;
        }

        public async Task<UserViewModel> Register(string username, string password)
        {
            UserViewModel user = null;

            CreateUserRequestModel requestModel = new CreateUserRequestModel()
            {
                EmailAddress = username,
                Password = password
            };

            APIResult result = await UsersAPIContext.Current.Create(requestModel);

            if (result.Success)
            {
                if (result.Data != null)
                {
                    UserResponseModel model = JsonConvert.DeserializeObject<UserResponseModel>(Convert.ToString(result.Data));

                    if (model != null)
                    {
                        user = UsersMapper.MapUserResponseModelToUserViewModel(model as UserResponseModel);

                        if (user.IsAuthenticated)
                        {
                            ((ClaimsIdentity)Context.HttpContext.User.Identity).AddClaim(new Claim(ClaimTypes.Thumbprint, user.SecurityToken));
                            ((ClaimsIdentity)Context.HttpContext.User.Identity).AddClaim(new Claim(ClaimTypes.Expiration, user.SecurityTokenExpires.ToBinary().ToString()));
                            await this.SignInManager.SignInAsync(user, false);
                        }
                    }
                    else
                    {
                        user = new UserViewModel { EmailAddress = username, IsAuthenticated = false };
                    }
                }
            }

            return user;
        }

        public async Task<APIResult> StartPasswordReset(string emailAddress)
        {
            StartPasswordResetRequestModel requestModel = new StartPasswordResetRequestModel()
            {
                EmailAddress = emailAddress
            };

            APIResult result = await UsersAPIContext.Current.StartPasswordReset(requestModel);

            return result;
        }

        public async Task<APIResult> FinishPasswordReset(string token, FinishPasswordResetRequestModel model)
        {
            APIResult result = await UsersAPIContext.Current.FinishPasswordReset(token, model);

            return result;
        }

        public async Task SignOut()
        {
            await this.SignInManager.SignOutAsync();
        }
    }
}
