﻿namespace digicard.Components.UI.Authentication
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using digicard.Components.UI;
    using digicard.Models.API.Request;
    using digicard.Models.API.Request.Users;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Components.Shared.API;
    using digicard.Models.UI.View.Users;

    public interface IAuthenticationManager
    {
        IHttpContextAccessor Context { get; }

        Task<UserViewModel> GetCurrentUser();

        Task<UserViewModel> SignIn(string username, string password, string ipAddress);

        Task<UserViewModel> Register(string username, string password);

        Task<APIResult> StartPasswordReset(string emailAddress);

        Task<APIResult> FinishPasswordReset(string token, FinishPasswordResetRequestModel model);

        Task SignOut();
    }
}
