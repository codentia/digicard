﻿namespace digicard.Components.UI.Authentication
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Options;
    using digicard;
    using digicard.Components.UI.API;
    using digicard.Components.Shared.API;
    using digicard.Models.API.Request.Sessions;

    public class SessionsAPIContext : DigicardAPIContext, ISessionsAPIContext
    {
        public SessionsAPIContext(IOptions<AppSettings> appSettings, IHttpContextAccessor context)
            : base(appSettings, context)
        {
        }

        public static ISessionsAPIContext Current { get; set; }

        public async Task<APIResult> CreateSession(CreateSessionRequestModel requestModel)
        {
            return await this.CallAPI(APIHttpMethod.Post, "sessions", requestModel);
        }

        public async Task<APIResult> RetrieveSession(string emailAddress, bool allowCache = false)
        {
            emailAddress = WebUtility.UrlEncode(emailAddress);
            return await this.CallAPI(APIHttpMethod.Get, $"sessions?username={emailAddress}", string.Empty);
        }

        public async Task<APIResult> EndSession(string emailAddress)
        {
            emailAddress = WebUtility.UrlEncode(emailAddress);
            return await this.CallAPI(APIHttpMethod.Delete, $"sessions?username={emailAddress}", string.Empty);
        }
    }
}
