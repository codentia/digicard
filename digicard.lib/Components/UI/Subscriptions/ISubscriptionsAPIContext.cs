namespace digicard.Components.UI.Subscriptions
{
    using System.Threading.Tasks;
    using digicard.Components.Shared.API;

    public interface ISubscriptionsAPIContext
    {
        Task<APIResult> GetBraintreeTokenForUser(string userId);

        Task<APIResult> UpdateSubscription();

        Task<APIResult> Cancel(string userId);
    }
}