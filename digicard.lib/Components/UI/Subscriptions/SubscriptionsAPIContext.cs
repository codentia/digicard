namespace digicard.Components.UI.Subscriptions
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Options;
    using digicard.Components.UI.API;
    using digicard.Components.Shared.API;

    public class SubscriptionsAPIContext : DigicardAPIContext, ISubscriptionsAPIContext
    {
        public SubscriptionsAPIContext(IOptions<AppSettings> appSettings, IHttpContextAccessor context)
            : base(appSettings, context)
        {
        }

        public static ISubscriptionsAPIContext Current { get; set; }


        public async Task<APIResult> GetBraintreeTokenForUser(string userId)
        {
            return await this.CallAPI(APIHttpMethod.Get, $"Subscriptions/GetBraintreeTokenForUser/${userId}", string.Empty);
        }

        public async Task<APIResult> UpdateSubscription()
        {
            return await this.CallAPI(APIHttpMethod.Post, $"Subscriptions/Update", string.Empty);
        }

        public async Task<APIResult> Cancel(string userId)
        {
            return await this.CallAPI(APIHttpMethod.Post, $"Subscriptions/Cancel/{userId}", string.Empty);
        }
    }
}