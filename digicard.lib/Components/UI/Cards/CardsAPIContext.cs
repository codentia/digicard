namespace digicard.Components.UI.Cards
{
    using digicard;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Options;
    using digicard.Components.UI.API;
    using digicard.Components.Shared.API;
    using digicard.Models.API.Request.Cards;

    public class CardsAPIContext : DigicardAPIContext, ICardsAPIContext
    {
        public CardsAPIContext(IOptions<AppSettings> appSettings, IHttpContextAccessor context)
            : base(appSettings, context)
        {
        }

        public static ICardsAPIContext Current { get; set; }


        public async Task<APIResult> Get(string id)
        {
            return await this.CallAPI(APIHttpMethod.Get, $"cards/{id}", string.Empty);
        }

        public async Task<APIResult> GetBySlug(string slug)
        {
            return await this.CallAPI(APIHttpMethod.Get, $"cards/slug/{slug}", string.Empty);
        }

        public async Task<APIResult> GetRandomN(int count)
        {
            return await this.CallAPI(APIHttpMethod.Get, $"cards/?count={count}", string.Empty);
        }

        public async Task<APIResult> Create(CreateCardRequestModel requestModel)
        {
            return await this.CallAPI(APIHttpMethod.Post, $"cards", requestModel);
        }

        public async Task<APIResult> Update(string id)
        {
            return await this.CallAPI(APIHttpMethod.Post, $"cards/{id}", string.Empty);
        }
    }
}