namespace digicard.Components.UI.Cards
{
    using System.Threading.Tasks;
    using digicard.Components.Shared.API;
    using digicard.Models.API.Request.Cards;

    public interface ICardsAPIContext
    {
        Task<APIResult> Get(string id);

        Task<APIResult> GetBySlug(string slug);

        Task<APIResult> GetRandomN(int count);

        Task<APIResult> Create(CreateCardRequestModel requestModel);

        Task<APIResult> Update(string id);
    }
}