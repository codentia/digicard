﻿namespace digicard.Components.UI.Accounts
{
    using System.Threading.Tasks;
    using digicard.Components.API;
    using digicard.Components.Shared.API;
    using digicard.Models.API.Request.Accounts;

    public interface IAccountsAPIContext
    {
        Task<APIResult> Create(CreateAccountRequestModel requestModel);

        Task<APIResult> Update(string id, UpdateAccountRequestModel requestModel);

        Task<APIResult> Get(string id);

        Task<APIResult> GetUsers(string id);
    }
}
