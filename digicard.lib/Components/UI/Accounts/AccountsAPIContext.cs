﻿namespace digicard.Components.UI.Accounts
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Options;
    using digicard;
    using digicard.Components.UI.API;
    using digicard.Components.Shared.API;
    using digicard.Models.API.Request.Accounts;

    public class AccountsAPIContext : DigicardAPIContext, IAccountsAPIContext
    {
        public AccountsAPIContext(IOptions<AppSettings> appSettings, IHttpContextAccessor context)
            : base(appSettings, context)
        {
        }

        public static IAccountsAPIContext Current { get; set; }

        public async Task<APIResult> Create(CreateAccountRequestModel requestModel)
        {
            return await this.CallAPI(APIHttpMethod.Post, $"accounts", requestModel);
        }

        public async Task<APIResult> Update(string id, UpdateAccountRequestModel requestModel)
        {
            return await this.CallAPI(APIHttpMethod.Put, $"accounts/{id}", requestModel);
        }

        public async Task<APIResult> Get(string id)
        {
            return await this.CallAPI(APIHttpMethod.Get, $"accounts/{id}", string.Empty);
        }

        public async Task<APIResult> GetUsers(string id)
        {
            return await this.CallAPI(APIHttpMethod.Get, $"accounts/{id}/users", string.Empty);
        }
    }
}
