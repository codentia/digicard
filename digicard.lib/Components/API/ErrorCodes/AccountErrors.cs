﻿namespace digicard.Components.API.ErrorCodes
{
    using System.Collections.Generic;

    public sealed class AccountErrors : Dictionary<int, string>
    {
        public static readonly int AccountIdIsRequired = BaseValue + 1;
        public static readonly int AccountIdIsNotValid = BaseValue + 2;
        public static readonly int AccountDoesNotExist = BaseValue + 3;
        public static readonly int NameIsRequired = BaseValue + 4;

        private static readonly int BaseValue = ErrorBuilder.BaseValues[typeof(AccountErrors)];

        private static AccountErrors _instance = new AccountErrors
        {
            { AccountIdIsRequired, "AccountId is required." },
            { AccountIdIsNotValid, "AccountId is not valid - {0}." },
            { AccountDoesNotExist, "Account does not exist - {0}." },
            { NameIsRequired, "Name is required." }
        };

        public static AccountErrors Instance => _instance;
    }
}
