namespace digicard.Components.API.ErrorCodes
{
    using System.Collections.Generic;

    public sealed class CardErrors : Dictionary<int, string>
    {
        public static readonly int CardIdIsRequired = BaseValue + 1;
        public static readonly int NameOnCardIsRequired = BaseValue + 2;
        public static readonly int DescriptionIsRequired = BaseValue + 3;
        public static readonly int AddressLine1IsRequired = BaseValue + 4;
        public static readonly int PostCodeIsRequired = BaseValue + 5;
        public static readonly int CountryIsRequired = BaseValue + 6;
        public static readonly int SlugIsRequired = BaseValue + 7;
        public static readonly int CardDoesNotExist = BaseValue + 8;
        public static readonly int CardIdIsNotValid = BaseValue + 9;

        private static readonly int BaseValue = ErrorBuilder.BaseValues[typeof(CardErrors)];

        private static CardErrors _instance = new CardErrors
        {
            { CardIdIsRequired, "Card id is required." },
            { NameOnCardIsRequired, "Name on card is required." },
            { DescriptionIsRequired, "Description is required." },
            { AddressLine1IsRequired, "Address line 1 is required." },
            { PostCodeIsRequired, "Post code is required." },
            { CountryIsRequired, "Country is required." },
            { SlugIsRequired, "Slug is required." },
            { CardDoesNotExist, "Card does not exist - {0}." },
            { CardIdIsNotValid, "Card id is not valid - {0}." },
        };

        public static CardErrors Instance => _instance;
    }
}
