﻿namespace digicard.Components.API.ErrorCodes
{
    using System.Collections.Generic;

    public sealed class AuthenticationErrors : Dictionary<int, string>
    {
        public static readonly int ExternalTokenIsRequired = BaseValue + 1;
        public static readonly int IPAddressIsRequired = BaseValue + 2;
        public static readonly int PasswordIsRequired = BaseValue + 3;
        public static readonly int AccountIdIsRequired = BaseValue + 4;
        public static readonly int AccountIdIsNotValid = BaseValue + 5;
        public static readonly int AccountDoesNotExist = BaseValue + 6;
        public static readonly int AccountIsNotBeingEnrolled = BaseValue + 7;
        public static readonly int UsernameIsRequired = BaseValue + 8;
        public static readonly int AuthenticationFailed = BaseValue + 9;

        private static readonly int BaseValue = ErrorBuilder.BaseValues[typeof(AuthenticationErrors)];

        private static AuthenticationErrors _instance = new AuthenticationErrors
        {
            { ExternalTokenIsRequired, "External Token is required." },
            { IPAddressIsRequired, "IP Address is required." },
            { PasswordIsRequired, "Password is required." },
            { AccountIdIsRequired, "AccountId is required." },
            { AccountIdIsNotValid, "AccountId is not valid - {0}." },
            { AccountDoesNotExist, "Account does not exist - {0}." },
            { AccountIsNotBeingEnrolled, "Account is not being enrolled - {0}." },
            { UsernameIsRequired, "Username is required." },
            { AuthenticationFailed, "Authentication Failed" },
        };

        public static AuthenticationErrors Instance => _instance;
    }
}
