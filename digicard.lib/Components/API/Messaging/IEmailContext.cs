﻿namespace digicard.Components.API.Messaging
{
    using System.Threading.Tasks;
    using digicard.Components.API.Accounts; 
    using digicard.Components.API.Users;

    public interface IEmailContext
    {
        Task SendStartPasswordResetLink(User user);

        Task SendPasswordChangedNotification(User user);

        Task SendEmailValidationLink(User user);
    }
}
