﻿namespace digicard.Components.API.Messaging
{
    using digicard.Components.API.Users;
    using digicard.Components.API.Accounts; 

    public class MessageParameters
    {
        public Account Account { get; set; }

        public User ToUser { get; set; }

        public User FromUser { get; set; }
    }
}
