﻿namespace digicard.Components.API.Messaging
{
    using System;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using digicard.Components.API.Accounts;
    using digicard.Components.API.Users;

    public class EmailContext : IEmailContext
    {
        private const string COMPONENT = "EmailContext";
        private const string T_SYS_START_PASSWORD_RESET = "SYS_START_PASSWORD_RESET";
        private const string T_SYS_FINISH_PASSWORD_RESET = "SYS_FINISH_PASSWORD_RESET";
        private const string T_SYS_CONFIRMEMAIL = "SYS_CONFIRMEMAIL";

        public static IEmailContext Current { get; set; }

        public async Task SendStartPasswordResetLink(User user)
        {
            await Send(user.EmailAddress, T_SYS_START_PASSWORD_RESET, new MessageParameters
            {
                ToUser = user,
            });
        }

        public async Task SendPasswordChangedNotification(User user)
        {
            await Send(user.EmailAddress, T_SYS_FINISH_PASSWORD_RESET, new MessageParameters
            {
                ToUser = user,
            });
        }

        public async Task SendEmailValidationLink(User user)
        {
            await Send(user.EmailAddress, T_SYS_CONFIRMEMAIL, new MessageParameters
            {
                ToUser = user,
            });
        }

        private async Task SendAdminNotification(string messageTemplateCode, MessageParameters parameters)
        {
            await this.Send(AppSettings.Current.AdminEmail, messageTemplateCode, parameters);
        }

        private async Task Send(string toAddress, string messageTemplateCode, MessageParameters parameters)
        {
            MessageTemplate template = await MessageTemplate.Get(messageTemplateCode);

            if (template == null)
            {
                throw new ArgumentException($"MessageTemplateCode '{messageTemplateCode}' does not resolve to a valid template", nameof(messageTemplateCode));
            }

            if (!string.IsNullOrEmpty(AppSettings.Current.OverrideToEmail))
            {
                toAddress = AppSettings.Current.OverrideToEmail;
            }

            await template.AddParameters(parameters);

            MailMessage message = new MailMessage(AppSettings.Current.MailFrom, toAddress)
            {
                Subject = template.GetSubject(),
                Body = template.GetBody(),
                IsBodyHtml = true,
            };

            SmtpClient client = new SmtpClient(AppSettings.Current.MailServer, AppSettings.Current.MailPort)
            {
                Credentials = new System.Net.NetworkCredential(AppSettings.Current.MailUsername, AppSettings.Current.MailPassword),
            };

            await client.SendMailAsync(message);

            await EmailDbContext.Current.SaveMessageHistory(template.MessageTemplateId, parameters.Account == null ? 0 : parameters.Account.AccountId, parameters.ToUser == null ? 0 : parameters.ToUser.UserId, toAddress, message.Body, message.Subject);
        }
    }
}
