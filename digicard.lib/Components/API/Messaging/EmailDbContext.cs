﻿namespace digicard.Components.API.Messaging
{
    using System;
    using System.Data;
    using System.Threading.Tasks;
    using Codentia.Common.Data;

    public class EmailDbContext : DatabaseContext, IEmailDbContext
    {
        private const string GetMessageTemplateQuery = "SELECT MessageTemplateId, MessageTemplateCode, IsHtml, IsAdmin, ParentTemplateId, Body, Subject FROM ( SELECT MessageTemplateId, MessageTemplateCode, IsHtml, IsAdmin, ParentTemplateId, Body, Subject FROM messagetemplate WHERE MessageTemplateCode = @MessageTemplateCode UNION ALL SELECT MessageTemplateId, MessageTemplateCode, IsHtml, IsAdmin, ParentTemplateId, Body, Subject FROM messagetemplate WHERE MessageTemplateId = (SELECT ParentTemplateId FROM messagetemplate WHERE MessageTemplateCode = @MessageTemplateCode) ) t ORDER BY COALESCE(ParentTemplateId, 999999);";
        private const string CreateMessageHistoryQuery = "INSERT INTO messagehistory (MessageTemplateId, AccountId, ToUserId, EmailAddress, Subject, Body) VALUES (@MessageTemplateId, NULLIF(@AccountId, 0), @ToUserId, @EmailAddress, @Subject, @Body);";
        private const string HasMessageBeenSentQuery = "SELECT CASE WHEN EXISTS ( SELECT 1 FROM messagehistory mh INNER JOIN messagetemplate mt ON mt.MessageTemplateId = mh.MessageTemplateId WHERE mh.AccountId = @AccountId AND mh.EmailAddress = @EmailAddress AND mt.MessageTemplateCode = @MessageTemplateCode) THEN 1 ELSE 0 END;";

        public static IEmailDbContext Current { get; set; }

        public async Task<DataTable> GetMessageTemplate(string messageTemplateCode)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@MessageTemplateCode", DbType.String, 100, messageTemplateCode),
            };

            return await this.Execute<DataTable>(GetMessageTemplateQuery, parameters);
        }

        public async Task SaveMessageHistory(int messageTemplateId, int accountId, int? toUserId, string emailAddress, string body, string subject)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@MessageTemplateId", DbType.Int32, messageTemplateId),
                new DbParameter("@AccountId", DbType.Int32, accountId),
                new DbParameter("@ToUserId", DbType.Int32, (toUserId == null || toUserId.Value == 0) ? (object)DBNull.Value : toUserId),
                new DbParameter("@EmailAddress", DbType.String, 250, emailAddress),
                new DbParameter("@Body", DbType.String, -1, body),
                new DbParameter("@Subject", DbType.String, -1, subject)
            };

            await this.Execute<DBNull>(CreateMessageHistoryQuery, parameters);
        }

        public async Task<bool> HasMessageBeenSent(int accountId, string messageTemplateCode, string emailAddress)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@AccountId", DbType.Int32, accountId),
                new DbParameter("@EmailAddress", DbType.String, 250, emailAddress),
                new DbParameter("@MessageTemplateCode", DbType.String, 100, messageTemplateCode),
            };

            return await this.Execute<bool>(HasMessageBeenSentQuery, parameters);
        }
    }
}
