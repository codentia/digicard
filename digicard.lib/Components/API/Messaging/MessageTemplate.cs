﻿namespace digicard.Components.API.Messaging
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;

    public class MessageTemplate
    {
        public MessageTemplate()
        {
            this.Tokens = new Dictionary<string, string>
            {
                { "SITE", AppSettings.Current.SitePrefix },
            };
        }

        public int MessageTemplateId { get; set; }

        public string MessageTemplateCode { get; set; }

        public bool IsHtml { get; set; }

        public bool IsAdmin { get; set; }

        public MessageTemplate ParentTemplate { get; set; }

        public string Body { get; set; }

        public string Subject { get; set; }

        private Dictionary<string, string> Tokens { get; set; }

        public static MessageTemplate FromDataRow(DataRow row, MessageTemplate parent = null)
        {
            return new MessageTemplate
            {
                MessageTemplateId = Convert.ToInt32(row["MessageTemplateId"]),
                MessageTemplateCode = Convert.ToString(row["MessageTemplateCode"]),
                IsHtml = Convert.ToBoolean(row["IsHtml"]),
                IsAdmin = Convert.ToBoolean(row["IsAdmin"]),
                ParentTemplate = parent,
                Body = Convert.ToString(row["Body"]),
                Subject = Convert.ToString(row["Subject"]),
            };
        }

        public static List<MessageTemplate> FromDataTable(DataTable table)
        {
            List<MessageTemplate> parents = new List<MessageTemplate>();
            List<MessageTemplate> templates = new List<MessageTemplate>();

            if (table != null)
            {
                foreach (DataRow r in table.Rows)
                {
                    int? parentTemplateId = null;

                    // first, load parent templates
                    if (r["ParentTemplateId"] != DBNull.Value)
                    {
                        int findParentId = Convert.ToInt32(r["ParentTemplateId"]);
                        parentTemplateId = findParentId;

                        if (!parents.Any(p => p.MessageTemplateId == findParentId))
                        {
                            DataRow row = table.AsEnumerable().First(t => t.Field<int>("MessageTemplateId") == findParentId);
                            parents.Add(MessageTemplate.FromDataRow(row));
                        }
                    }

                    // now the template itself (unless it's a parent)
                    if (!parents.Any(p => p.MessageTemplateId == Convert.ToInt32(r["MessageTemplateId"])))
                    {
                        templates.Add(MessageTemplate.FromDataRow(r, parents.FirstOrDefault(p => p.MessageTemplateId == parentTemplateId)));
                    }
                }
            }

            return templates;
        }

        public static async Task<MessageTemplate> Get(string messageTemplateCode)
        {
            MessageTemplate template = null;

            if (!string.IsNullOrEmpty(messageTemplateCode))
            {
                DataTable data = await EmailDbContext.Current.GetMessageTemplate(messageTemplateCode);
                template = MessageTemplate.FromDataTable(data).FirstOrDefault();
            }

            return template;
        }

        public string GetSubject(string currentContent = "")
        {
            string result = this.ReplaceTokensInTemplate(this.Subject, currentContent);

            if (this.ParentTemplate != null)
            {
                result = this.ParentTemplate.GetSubject(currentContent: result);
            }

            return result;
        }

        public string GetBody(string currentContent = "")
        {
            string result = this.ReplaceTokensInTemplate(this.Body, currentContent);

            if (this.ParentTemplate != null)
            {
                result = this.ParentTemplate.GetBody(currentContent: result);
            }

            return result;
        }

        public async Task AddParameters(MessageParameters parameters)
        {
            this.Tokens.Add("SITE_PREFIX", AppSettings.Current.SitePrefix);

            if (parameters.ToUser != null)
            {
                this.Tokens.Add("USER_EMAIL", parameters.ToUser.EmailAddress);
                this.Tokens.Add("USER_TOKEN", parameters.ToUser.SecurityToken);
            }

            if (parameters.FromUser != null)
            {
                this.Tokens.Add("FROM_USER_EMAIL", parameters.FromUser.EmailAddress);
            }

            if (this.ParentTemplate != null)
            {
                await this.ParentTemplate.AddParameters(parameters);
            }
        }

        private string ReplaceTokensInTemplate(string template, string currentContent = "")
        {
            string result = template;

            if (string.IsNullOrEmpty(template) && !string.IsNullOrEmpty(currentContent))
            {
                result = currentContent;
            }
            else
            {
                foreach (string token in this.Tokens.Keys)
                {
                    result = result.Replace($"[[{token}]]", this.Tokens[token]);
                }

                if (!string.IsNullOrEmpty(currentContent))
                {
                    result = result.Replace("[[MAILTEXT]]", currentContent);
                }
            }

            return result;
        }
    }
}
