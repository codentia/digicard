﻿namespace digicard.Components.API.Messaging
{
    using System;
    using System.Data;
    using System.Threading.Tasks;

    public interface IEmailDbContext
    {
        Task<DataTable> GetMessageTemplate(string messageTemplateCode);

        Task<bool> HasMessageBeenSent(int workspaceId, string messageTemplateCode, string emailAddress);

        Task SaveMessageHistory(int messageTemplateId, int workspaceId, int? toUserId, string emailAddress, string body, string subject);
    }
}
