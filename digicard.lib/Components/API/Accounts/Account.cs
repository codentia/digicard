namespace digicard.Components.API.Accounts
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using digicard.Components.Shared.Logging;
    using digicard.Components.API.Users;

    public class Account
    {
        public int AccountId { get; set; }

        public int UserId { get; set; }
        
        public string Name { get; set; }

        public string BrainTreeSubId { get; set; }

        public string BrainTreeCustomerId { get; set; }

        public DateTime SubscriptionExpiry { get; set; }

        public DateTime? SubscriptionCancelledAt { get; set; }


        public static async Task<Account> Get(int accountId)
        {
            DataRow data = await AccountsDbContext.Current.GetAccount(accountId);
            return Account.FromDataRow(data);
        }

        public static List<Account> FromDataTable(DataTable data)
        {
            List<Account> accounts = new List<Account>();

            if (data != null)
            {
                accounts = data.AsEnumerable().Select(r => Account.FromDataRow(r)).ToList();
            }

            return accounts;
        }

        public static Account FromDataRow(DataRow data)
        {
            if (data == null) return null;

            return new Account
            {
                AccountId = Convert.ToInt32(data["AccountId"]),
                UserId = Convert.ToInt32(data["UserId"]),
                Name = Convert.ToString(data["AccountName"]),
                BrainTreeSubId = data["BraintreeSubscriptionId"] == DBNull.Value ? string.Empty : Convert.ToString(data["BraintreeSubscriptionId"]),
                BrainTreeCustomerId = data["BraintreeCustromerId"] == DBNull.Value ? string.Empty : Convert.ToString(data["BraintreeCustromerId"]),
                SubscriptionExpiry = Convert.ToDateTime(data["SubscriptionExpiry"]),
                SubscriptionCancelledAt = data["SubscriptionCancelledAt"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(data["SubscriptionCancelledAt"])
            };
        }

        public async Task Save()
        {
            if (this.AccountId == 0)
            {
                this.AccountId = await AccountsDbContext.Current.CreateAccount(this.UserId, this.Name);
                await AuditLogEntry.Write(this, null, AuditOperationType.Create, AuditObjectType.Account, this.AccountId);
            }
            else
            {
                await AccountsDbContext.Current.UpdateAccount(this.AccountId);
                await AuditLogEntry.Write(this, null, AuditOperationType.Update, AuditObjectType.Account, this.AccountId);
            }
        }
    }
}
