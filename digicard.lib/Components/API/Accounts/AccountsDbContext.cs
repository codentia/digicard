﻿namespace digicard.Components.API.Accounts
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Codentia.Common.Data;

    public class AccountsDbContext : DatabaseContext, IAccountsDbContext
    {
        private const string COLS_ACCOUNT = "a.AccountId, a.UserId, a.AccountName, a.BraintreeSubscriptionId, a.BraintreeCustromerId, a.SubscriptionExpiry, a.SubscriptionCancelledAt";
        private const string CreateAccountQuery = "INSERT INTO account (UserId, AccountName, SubscriptionExpiry) VALUES (@UserId, @AccountName, NOW() - INTERVAL 1 WEEK); SELECT LAST_INSERT_ID();";
        private const string UpdateAccountQuery = "UPDATE account WHERE AccountId = @AccountId;";

        private static readonly string GetAccountByIdQuery = $"SELECT {COLS_ACCOUNT} FROM account a WHERE AccountId = @AccountId";

        // private static readonly string GetAccountsQuery = $"SELECT {COLS_ACCOUNT} FROM account a INNER JOIN plantype p ON w.PlanTypeId = p.PlanTypeId WHERE ";
     
        public static IAccountsDbContext Current { get; set; }

        public async Task<DataRow> GetAccount(int accountId)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@AccountId", DbType.Int32, accountId)
            };

            DataTable result = await this.Execute<DataTable>(GetAccountByIdQuery, parameters);
            return result.AsEnumerable().FirstOrDefault();
        }

        public async Task<int> CreateAccount(int userId, string accountName)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@UserId", DbType.Int32, userId),
                new DbParameter("@AccountName", DbType.String, accountName)
            };

            return await this.Execute<int>(CreateAccountQuery, parameters);
        }

        public async Task UpdateAccount(int accountId)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@AccountId", DbType.Int32, accountId)
            };

            await this.Execute<DBNull>(UpdateAccountQuery, parameters);
        }
    }
}
