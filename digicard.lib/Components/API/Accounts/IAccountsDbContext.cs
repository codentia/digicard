﻿namespace digicard.Components.API.Accounts
{
    using System;
    using System.Data;
    using System.Threading.Tasks;

    public interface IAccountsDbContext
    {
        Task<DataRow> GetAccount(int accountId);

        Task<int> CreateAccount(int userId, string accountName);

        Task UpdateAccount(int accountId);
    }
}
