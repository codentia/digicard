﻿namespace digicard.Components
{
    public abstract class DatabaseContext : Codentia.Common.Data.DbContext<AppSettings>
    {
        protected DatabaseContext()
            : base("digicard")
        {
        }
    }
}
