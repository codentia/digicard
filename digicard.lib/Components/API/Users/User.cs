namespace digicard.Components.API.Users
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.IdentityModel.Tokens;
    using digicard;
    using digicard.Components.API.Accounts;
    using digicard.Components.API.Authentication;
    using digicard.Components.API.Messaging;
    using digicard.Components.Shared.Logging;

    public class User
    {
        private const string COMPONENT = "User";

        public User()
        {
            this.IsAuthenticated = false;
        }

        public Account Account { get; set; }

        public Account CurrentAccount => Account;

        public int UserId { get; set; }

        public string SecurityToken { get; set; }

        public DateTime SecurityTokenExpires { get; set; }

        public string PasswordHash { get; set; }

        public string EmailAddress { get; set; }

        public DateTime? EmailValidatedAt { get; set; }

        public bool IsAuthenticated { get; set; }

        public string IPAddress { get; set; }

        public bool IsEmailValidated => EmailValidatedAt != null;

        public static User FromDataRow(DataRow userData)
        {
            return new User
            {
                UserId = Convert.ToInt32(userData["UserId"]),
                IsAuthenticated = true,
                PasswordHash = Convert.ToString(userData["PasswordHash"]),
                EmailAddress = Convert.ToString(userData["EmailAddress"]),
                SecurityToken = Convert.ToString(userData["ExternalToken"]),
                SecurityTokenExpires = userData["ExternalTokenExpiry"] == DBNull.Value ? DateTime.Now.AddDays(-1) : Convert.ToDateTime(userData["ExternalTokenExpiry"]),
                EmailValidatedAt = userData["EmailValidatedAt"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(userData["EmailValidatedAt"])
            };
        }

        public static List<User> FromDataTable(DataTable userData)
        {
            List<User> users = new List<User>();

            foreach (DataRow r in userData.Rows)
            {
                users.Add(FromDataRow(r));
            }

            return users;
        }

        public static async Task<User> Get(string emailAddress)
        {
            User user = null;

            DataRow userData = await UsersDbContext.Current.GetUser(emailAddress);

            if (userData != null)
            {
                user = FromDataRow(userData);
            }

            return user;
        }

        public static async Task<User> Get(int userId)
        {
            User user = null;

            if (userId > 0)
            {
                // get the user
                DataRow userData = await UsersDbContext.Current.GetUser(userId);

                if (userData != null)
                {
                    user = FromDataRow(userData);
                }
            }

            return user;
        }

        public static async Task<User> GetByToken(string externalToken)
        {
            User user = null;

            DataTable userData = await UsersDbContext.Current.GetUserByToken(externalToken);

            if (userData.Rows.Count == 1)
            {
                user = User.FromDataRow(userData.Rows[0]);
            }

            return user;
        }

        public static async Task<List<User>> GetAll()
        {
            DataTable userData = await UsersDbContext.Current.GetUsers();

            return User.FromDataTable(userData);
        }

        public static async Task<Tuple<User, LoginResponseReason>> Authenticate(string emailAddress, string password, string ipAddress)
        {
            LoginResponseReason loginReason = LoginResponseReason.LoginOK;
            User user = await User.Get(emailAddress);

            // calculate the hash and compare
            if (user != null)
            {
                user.IsAuthenticated = PasswordManager.ComparePasswordHash(user.PasswordHash, password);

                if (user.IsAuthenticated)
                {
                    user.IPAddress = ipAddress;
                    user.GenerateSecurityToken();
                }
                else
                {
                    loginReason = LoginResponseReason.FailedLoginPasswordMismatch;
                }
            }
            else
            {
                loginReason = LoginResponseReason.FailedLoginEmailNotInSystem;
            }

            return new Tuple<User, LoginResponseReason>(user, loginReason);
        }

        public static async Task FinishEmailValidation(string token)
        {
            await UsersDbContext.Current.SetEmailValidated(token);
        }

        public void GenerateSecurityToken(bool invalidatingToken = false)
        {
            if (this.IsAuthenticated)
            {
                DateTime tokenExpiry = DateTime.Now.AddMinutes(60);

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, this.EmailAddress),
                    new Claim(JwtRegisteredClaimNames.Jti, this.UserId.ToString()),
                    new Claim(ClaimTypes.Expiration, tokenExpiry.ToBinary().ToString()),
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppSettings.Current.JWTKey));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                JwtSecurityToken token = new JwtSecurityToken(issuer: AppSettings.Current.JWTIssuer, audience: AppSettings.Current.JWTIssuer, claims: claims, expires: tokenExpiry, signingCredentials: creds);

                this.SecurityToken = new JwtSecurityTokenHandler().WriteToken(token);
                this.SecurityTokenExpires = invalidatingToken ? DateTime.Now.AddMinutes(-1) : tokenExpiry;
            }
        }

        public async Task<bool> StartPasswordReset()
        {
            // 1. reset the token of this workspace to secure it
            this.SecurityToken = Guid.NewGuid().ToString();
            await UsersDbContext.Current.UpdatePasswordAndToken(this.UserId, this.PasswordHash, this.SecurityToken);

            // 2. generate and send reset link
            await EmailContext.Current.SendStartPasswordResetLink(this);

            return true;
        }

        public async Task<bool> FinishPasswordReset(string token, string passwordHash)
        {
            if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(passwordHash))
            {
                await UsersDbContext.Current.UpdatePassword(token, passwordHash);
                await EmailContext.Current.SendPasswordChangedNotification(this);

                return true;
            }

            return false;
        }

        public async Task StartEmailValidation()
        {
            // 1. reset the token of this workspace to secure it
            string token = Guid.NewGuid().ToString();
            await UsersDbContext.Current.UpdatePasswordAndToken(this.UserId, this.PasswordHash, token);

            // 2. generate and send validation link
            await EmailContext.Current.SendEmailValidationLink(this);
        }

        public async Task<string> GetExternalToken(bool regenerate = true)
        {
            string token = string.Empty;

            if (regenerate)
            {
                token = Guid.NewGuid().ToString();
                await UsersDbContext.Current.UpdateToken(this.UserId, token);
            }
            else
            {
                token = await UsersDbContext.Current.GetToken(this.UserId);
            }

            return token;
        }

        public async Task FinishPasswordReset(string newPassword)
        {
            await Task.Delay(0);
            this.PasswordHash = PasswordManager.GenerateSaltedHash(newPassword, PasswordManager.GenerateSaltValue());
        }

        public async Task Save()
        {
            if (this.UserId == 0)
            {
                this.UserId = await UsersDbContext.Current.CreateUser(this.EmailAddress, this.PasswordHash, Guid.NewGuid().ToString());
                await LogEntry.Write(COMPONENT, "Save", $"Created Account {this.UserId}", severity: 1, userId: this.UserId);
                await this.StartEmailValidation();
                // await EmailContext.Current.SendAdminNotificationNewAccountCreated(this);
                await AuditLogEntry.Write(null, this, AuditOperationType.Create, AuditObjectType.User, this.UserId);
            }
            else
            {
                await UsersDbContext.Current.UpdateUser(this.UserId, this.EmailAddress, this.PasswordHash);
                await LogEntry.Write(COMPONENT, "Save", $"Updated User {this.UserId}", severity: 1, userId: this.UserId);
                await AuditLogEntry.Write(null, this, AuditOperationType.Update, AuditObjectType.User, this.UserId);
            }
        }
    }
}
