namespace digicard.Components.API.Cards
{
    using System;
    using System.Data;
    using System.Threading.Tasks;

    public interface ICardsDbContext
    {
        Task<DataTable> GetRandomCards(int count);

        Task<DataTable> GetByAccountId(int accountId);

        Task<DataRow> GetCard(int cardId);

        Task<DataRow> GetBySlug(string slug);

        Task<int> CreateCard(int userId, string nameOnCard, string slug, string description, string mobile, string landline, string email, string facebook, string instagram, string twitter, string domain, string addressLine1, string addressLine2, string addressLine3, string postCode, string country);

        Task UpdateCard(int cardId);

        Task DeleteCard(int cardId);
    }
}