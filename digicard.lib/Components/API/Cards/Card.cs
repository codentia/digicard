namespace digicard.Components.API.Cards
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using digicard.Components.Shared.Logging;
    using digicard.Components.API.Users;

    public class Card
    {
        public Card()
        {

        }

        public int CardId { get; set; }

        public int UserId { get; set; }
        
        public string NameOnCard { get; set; }

        public string Description { get; set; }

        public string Mobile { get; set; }

        public string Landline { get; set; }

        public string Email { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; }

        public string Instagram { get; set; }

        public string Domain { get; set; }

        public string Logo { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string PostCode { get; set; }

        public string Country { get; set; }

        public bool IsPublished { get; set; }

        public static async Task<List<Card>> GetRandom(int count)
        {
            DataTable data = await CardsDbContext.Current.GetRandomCards(count);
            return Card.FromDataTable(data);
        }

        public static async Task<List<Card>> GetByAccountId(int accountId)
        {
            DataTable data = await CardsDbContext.Current.GetByAccountId(accountId);
            return Card.FromDataTable(data);
        }

        public static async Task<Card> Get(int cardId)
        {
            DataRow data = await CardsDbContext.Current.GetCard(cardId);
            return Card.FromDataRow(data);
        }

        public static async Task<Card> GetBySlug(string slug)
        {
            DataRow data = await CardsDbContext.Current.GetBySlug(slug);
            return Card.FromDataRow(data);
        }

        public static List<Card> FromDataTable(DataTable data)
        {
            List<Card> cards = new List<Card>();

            if (data != null)
            {
                cards = data.AsEnumerable().Select(r => Card.FromDataRow(r)).ToList();
            }

            return cards;
        }

        public static Card FromDataRow(DataRow data)
        {
            if (data == null) return null;

            return new Card
            {
                CardId = Convert.ToInt32(data["CardId"]),
                UserId = Convert.ToInt32(data["UserId"]),
                NameOnCard = Convert.ToString(data["NameOnCard"]),
                Description = Convert.ToString(data["Description"]),
                Mobile = Convert.ToString(data["Mobile"]),
                Landline = Convert.ToString(data["Landline"]),
                Email = Convert.ToString(data["Email"]),
                Facebook = Convert.ToString(data["Facebook"]),
                Twitter = Convert.ToString(data["Twitter"]),
                Instagram = Convert.ToString(data["Instagram"]),
                Domain = Convert.ToString(data["Domain"]),
                Logo = Convert.ToString(data["Logo"]),
                AddressLine1 = Convert.ToString(data["AddressLine1"]),
                AddressLine2 = Convert.ToString(data["AddressLine2"]),
                AddressLine3 = Convert.ToString(data["AddressLine3"]),
                PostCode = Convert.ToString(data["PostCode"]),
                Country = Convert.ToString(data["Country"]),
                IsPublished = Convert.ToBoolean(data["IsPublished"])
            };
        }

        public async Task Delete()
        {
            await CardsDbContext.Current.DeleteCard(this.CardId);
        }

        public async Task Save()
        {
            if (this.CardId == 0)
            {
                string slug = this.NameOnCard.Replace(' ', '-').ToLower();
                this.CardId = await CardsDbContext.Current.CreateCard(this.UserId, this.NameOnCard, slug, this.Description, this.Mobile, this.Landline, this.Email, this.Facebook, this.Instagram, this.Twitter, this.Domain, this.AddressLine1, this.AddressLine2, this.AddressLine3, this.PostCode, this.Country);
                // await AuditLogEntry.Write(this, null, AuditOperationType.Create, AuditObjectType.Account, this.CardId);
            }
            else
            {
                await CardsDbContext.Current.UpdateCard(this.CardId);
                // await AuditLogEntry.Write(this, null, AuditOperationType.Update, AuditObjectType.Account, this.AccountId);
            }
        }
    }
}
