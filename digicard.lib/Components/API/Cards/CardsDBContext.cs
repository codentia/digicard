namespace digicard.Components.API.Cards
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Codentia.Common.Data;

    public class CardsDbContext : DatabaseContext, ICardsDbContext
    {
        private static string COLS_CARD = "c.CardId, c.UserId, c.NameOnCard, c.Description, c.Mobile, c.Landline, c.Email, c.Facebook, c.Instagram, c.Twitter, c.Domain, c.Logo, c.AddressLine1, c.AddressLine2, c.AddressLine3, c.PostCode, c.Country, c.IsPublished";
        private static string CreateCardQuery = "INSERT INTO card (UserId, NameOnCard, Slug, Description, Mobile, Landline, Email, Facebook, Instagram, Twitter, Domain, AddressLine1, AddressLine2, AddressLine3, PostCode, Country, IsPublished) VALUES (@UserId, @NameOnCard, @Slug, @Description, @Mobile, @Landline, @Email, @Facebook, @Instagram, @Twitter, @Domain, @AddressLine1, @AddressLine2, @AddressLine3, @PostCode, @Country, 0); SELECT LAST_INSERT_ID();";
        private static string UpdateCardQuery = "UPDATE card WHERE CardId = @CardId;";
        private static readonly string GetCardQuery = $"SELECT {COLS_CARD} FROM digicard.card c ";
        private static readonly string GetRandomCardsQuery = $"SELECT {COLS_CARD} FROM digicard.card c ORDER BY RAND() LIMIT @Count";
        private static readonly string GetByAccountIdQuery = $"SELECT {COLS_CARD} FROM digicard.card c INNER JOIN account a on a.UserId = c.UserId WHERE AccountId = @AccountId";
        private const string DeleteCardQuery = "DELETE FROM card WHERE CardId = @CardId;";
     
        public static ICardsDbContext Current { get; set; }

        public async Task<DataTable> GetRandomCards(int count)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@Count", DbType.Int32, count)
            };

            return await this.Execute<DataTable>(GetRandomCardsQuery, parameters);
        }

        public async Task<DataTable> GetByAccountId(int accountId)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@AccountId", DbType.Int32, accountId)
            };

            return await this.Execute<DataTable>(GetByAccountIdQuery, parameters);
        }


        public async Task<DataRow> GetCard(int cardId)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@CardId", DbType.Int32, cardId)
            };

            DataTable result = await this.Execute<DataTable>(GetCardQuery, parameters);
            return result.AsEnumerable().FirstOrDefault();
        }

        public async Task<DataRow> GetBySlug(string slug)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@Slug", DbType.String, slug)
            };

            string query = GetCardQuery + "WHERE Slug = @Slug OR Domain = @slug";

            DataTable result = await this.Execute<DataTable>(query, parameters);
            return result.AsEnumerable().FirstOrDefault();
        }

        public async Task<int> CreateCard(int userId, string nameOnCard, string slug, string description, string mobile, string landline, string email, string facebook, string instagram, string twitter, string domain, string addressLine1, string addressLine2, string addressLine3, string postCode, string country)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@UserId", DbType.Int32, userId),
                new DbParameter("@NameOnCard", DbType.String, 100, nameOnCard),
                new DbParameter("@Slug", DbType.String, 100, slug),
                new DbParameter("@Description", DbType.String, 255, description),
                new DbParameter("@Mobile", DbType.String, 30, mobile),
                new DbParameter("@Landline", DbType.String, 30, landline),
                new DbParameter("@Email", DbType.String, 150, email),
                new DbParameter("@Facebook", DbType.String, 255, facebook),
                new DbParameter("@Instagram", DbType.String, 255, instagram),
                new DbParameter("@Twitter", DbType.String, 255, twitter),
                new DbParameter("@Domain", DbType.String, 255, domain),
                new DbParameter("@AddressLine1", DbType.String, 100, addressLine1),
                new DbParameter("@AddressLine2", DbType.String, 100, addressLine2),
                new DbParameter("@AddressLine3", DbType.String, 100, addressLine3),
                new DbParameter("@PostCode", DbType.String, 10, postCode),
                new DbParameter("@Country", DbType.String, 30, country)
            };

            return await this.Execute<int>(CreateCardQuery, parameters);
        }

        public async Task UpdateCard(int cardId)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@CardId", DbType.Int32, cardId)
            };

            await this.Execute<DBNull>(UpdateCardQuery, parameters);
        }

        public async Task DeleteCard(int cardId)
        {
            DbParameter[] parameters =
            {
                new DbParameter("@CardId", DbType.Int32, cardId)
            };

            await this.Execute<DBNull>(DeleteCardQuery, parameters);
        }
    }
}
