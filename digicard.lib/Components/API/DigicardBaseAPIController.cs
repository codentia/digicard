﻿namespace digicard.Components.API
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using digicard.Components.API;
    using digicard.Components.API.Accounts; 
    using digicard.Components.API.Authentication;
    using digicard.Components.API.Cards;
    using digicard.Components.API.Messaging;
    using digicard.Components.API.Users;
    using digicard.Components.Cron;
    using digicard.Components.Shared.Logging;

    public class DigicardBaseAPIController : Controller
    {
        private User _currentUser;

        public DigicardBaseAPIController(IOptions<AppSettings> appSettings, IServiceProvider serviceProvider, IDistributedCache cache)
        {
            if (appSettings == null)
            {
                throw new ArgumentNullException(nameof(appSettings));
            }

            // set properties
            AppSettings.Current = appSettings.Value;
            ServiceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));

            // prep contexts
            if (AuthenticationDbContext.Current == null)
            {
                AuthenticationDbContext.Current = ServiceProvider.GetService<IAuthenticationDbContext>();
            }

            if (AccountsDbContext.Current == null)
            {
                AccountsDbContext.Current = ServiceProvider.GetService<IAccountsDbContext>();
            }

            if (EmailContext.Current == null)
            {
                EmailContext.Current = ServiceProvider.GetService<IEmailContext>();
            }

            if (EmailDbContext.Current == null)
            {
                EmailDbContext.Current = ServiceProvider.GetService<IEmailDbContext>();
            }

            if (LoggingDbContext.Current == null)
            {
                LoggingDbContext.Current = ServiceProvider.GetService<ILoggingDbContext>();
            }

            if (LoggingMqContext.Current == null)
            {
                LoggingMqContext.Current = ServiceProvider.GetService<ILoggingMqContext>();
            }

            if (UsersDbContext.Current == null)
            {
                UsersDbContext.Current = ServiceProvider.GetService<IUsersDbContext>();
            }

            if (CardsDbContext.Current == null)
            {
                CardsDbContext.Current = ServiceProvider.GetService<ICardsDbContext>();
            }
        }

        public User CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        Claim jti = User.FindFirst(JwtRegisteredClaimNames.Jti);

                        if (jti != null)
                        {
                            var t = Task.Run(async () => { return await Users.User.Get(Convert.ToInt32(jti.Value)); });
                            t.Wait();
                            _currentUser = t.Result;
                        }
                    }
                }

                return _currentUser;
            }
        }

        protected IServiceProvider ServiceProvider { get; set; }

        protected string Component
        {
            get
            {
                return Convert.ToString(this.ControllerContext.RouteData.Values["controller"]);
            }
        }

        protected string Method
        {
            get
            {
                return Convert.ToString(this.ControllerContext.RouteData.Values["action"]);
            }
        }
    }
}
