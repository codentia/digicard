﻿namespace digicard
{
    public enum APIHttpMethod
    {
        Get = 1,

        Post = 2,

        Put = 3,

        Delete = 4,

        Patch = 5,
    }
}
