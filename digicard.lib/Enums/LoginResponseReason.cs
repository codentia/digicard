﻿namespace digicard
{
    public enum LoginResponseReason
    {
        LoginOK,

        FailedLoginEmailNotInSystem,

        FailedLoginPasswordMismatch,
    }
}
