﻿namespace digicard
{
    public enum AuditOperationType
    {
        Create,

        Update,

        Delete,
    }
}
