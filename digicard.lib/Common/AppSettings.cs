namespace digicard
{
    using System;

    public class AppSettings
    {
        public AppSettings()
        {
        }

        public static AppSettings Current { get; set; }

        public string AdminEmail { get; set; }

        public string OverrideToEmail { get; set; }

        public string SMTPServer { get; set; }

        public string SMTPUser { get; set; }

        public string SMTPPort { get; set; }

        public string MailServer { get; set; }

        public int MailPort => 587;

        public string MailUsername { get; set; }

        public string MailPassword { get; set; }

        public string MailFrom { get; set; }

        public string SitePrefix { get; set; }

        public string APIUrl { get; set; }
        
        public string RedisConnectionString { get; set; }

        public string RedisKey { get; set; }
        
        public string MessageQueueRoutingKey { get; set; }

        public string MessageQueueHost { get; set; }

        public string MessageQueueUsername { get; set; }

        public string MessageQueuePassword { get; set; }

        public string JWTKey { get; set; }

        public string JWTIssuer { get; set; }

        public string BaseDomain { get; set; }
    }
}
