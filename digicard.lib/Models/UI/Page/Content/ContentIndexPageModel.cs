﻿using System.Security.AccessControl;
using System.Collections.Generic;
using digicard.Models.UI.View.Cards;

namespace digicard.Models.UI.Page.Content
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;

    public class ContentIndexPageModel : BasePageModel
    {
        public ContentIndexPageModel()
        {
            this.Examples = new List<CardViewModel>();
        }

        public List<CardViewModel> Examples { get; set; }
    }
}
