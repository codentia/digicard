﻿using digicard.Models.UI.View.Accounts;

namespace digicard.Models.UI.Page.Accounts
{
    using Microsoft.AspNetCore.Http;

    public class AccountsNewPageModel : BasePageModel
    {
        public AccountsNewPageModel()
        {
            this.NewAccount = new AccountViewModel();
        }

        public AccountViewModel NewAccount { get; set; }
    }
}
