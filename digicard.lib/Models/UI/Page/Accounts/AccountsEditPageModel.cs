﻿namespace digicard.Models.UI.Page.Accounts
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.View.Accounts;
    using digicard.Models.UI.View.Users;

    public class AccountsEditPageModel : BasePageModel
    {
        public AccountsEditPageModel()
        {
        }

        public AccountViewModel Account { get; set; }
    }
}
