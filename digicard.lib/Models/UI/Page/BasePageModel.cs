﻿namespace digicard.Models.UI.Page 
{
    using System.Collections.Generic;
    using digicard.Models.UI.View.Accounts;
    using digicard.Models.UI.View.Users;
    using digicard.Models.UI.Page.Authentication;
    using digicard.Models.UI.Page.Users;

    public class BasePageModel
    {
        public BasePageModel()
        {
            this.ErrorMessages = new List<string>();
            this.AlertMessages = new List<string>();
        }

        public List<string> ErrorMessages { get; set; }

        public List<string> AlertMessages { get; set; }

        public AccountViewModel CurrentAccount { get; set; }

        public UserViewModel CurrentUser { get; set; }
    }
}
