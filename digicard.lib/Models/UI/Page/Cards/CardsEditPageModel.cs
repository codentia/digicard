using digicard.Models.UI.View.Cards;

namespace digicard.Models.UI.Page.Cards
{
    using Microsoft.AspNetCore.Http;

    public class CardsEditPageModel : BasePageModel
    {
        public CardsEditPageModel()
        {
            this.Card = new CardViewModel();
        }

        public CardViewModel Card { get; set; }
    }
}
