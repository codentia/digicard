using System;
using System.Collections.Generic;
using digicard.Models.UI.View.Cards;

namespace digicard.Models.UI.Page.Cards
{
    using Microsoft.AspNetCore.Http;

    public class CardsIndexPageModel : BasePageModel
    {
        public CardsIndexPageModel()
        {
            this.Cards = new List<CardViewModel>()
            {
                new CardViewModel
                {
                    CardId = 1,
                    UserId = 2,
                    NameOnCard = "Eriks Card",
                    Description = "This is Eriks Card",
                    Landline = "01534 748620",
                    Mobile = "07700785555",
                    Email = "matt@code.je",
                    Facebook = "http://facebook.com/codeje",
                    Twitter = "@mattcjsy",
                    Instagram = "http://insta.gram.link.here",
                    Domain = "test.com",
                    AddressLine1 = "Address1",
                    AddressLine2 = "Address2",
                    AddressLine3 = "Address3",
                    PostCode = "JE2 3WA",
                    Country = "Jersey",
                    IsPublished = true
                }
            };
        }

        public List<CardViewModel> Cards { get; set; }
    }
}
