using digicard.Models.UI.View.Cards;

namespace digicard.Models.UI.Page.Cards
{
    using Microsoft.AspNetCore.Http;

    public class CardsDetailPageModel : BasePageModel
    {
        public CardsDetailPageModel()
        {
            this.Card = new CardViewModel();
        }

        public CardViewModel Card { get; set; }
    }
}
