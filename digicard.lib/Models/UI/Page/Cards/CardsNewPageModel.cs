using digicard.Models.UI.View.Cards;

namespace digicard.Models.UI.Page.Cards
{
    using Microsoft.AspNetCore.Http;

    public class CardsNewPageModel : BasePageModel
    {
        public CardsNewPageModel()
        {
            this.NewCard = new CardViewModel();
        }

        public CardViewModel NewCard { get; set; }
    }
}
