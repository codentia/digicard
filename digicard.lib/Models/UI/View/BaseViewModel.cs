﻿namespace digicard.Models.UI.View
{
    using System.Collections.Generic;
    using digicard.Models.UI.View.Accounts;
    using digicard.Models.UI.View.Users;

    public class BaseViewModel
    {
        public BaseViewModel()
        {
            this.ErrorMessages = new List<string>();
            this.AlertMessages = new List<string>();
        }

        public List<string> ErrorMessages { get; set; }

        public List<string> AlertMessages { get; set; }

        public AccountViewModel CurrentAccount { get; set; }
    }
}
