﻿namespace digicard.Models.UI.View.Cards
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class CardViewModel
    {
        public int CardId { get; set; }

        public int UserId { get; set; }

        [Required]
        public string NameOnCard { get; set; }

        [Required]
        public string Description { get; set; }

        public string Landline { get; set; }

        public string Logo { get; set; }

        public string LogoExtension { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; } 

        public string Instagram { get; set; } 

        public string Domain { get; set; } 

        [Required]
        public string AddressLine1 { get; set; } 

        public string AddressLine2 { get; set; } 

        public string AddressLine3 { get; set; } 

        [Required]
        public string PostCode { get; set; } 

        [Required]
        public string Country { get; set; } 

        public bool IsPublished { get; set; } 
    }
}
