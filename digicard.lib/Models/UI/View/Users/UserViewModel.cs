namespace digicard.Models.UI.View.Users
{
    using System;

    public class UserViewModel : BaseViewModel
    {
        public int UserId { get; set; }

        public string EmailAddress { get; set; }        
        
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public bool IsAuthenticated { get; set; }

        public bool IsEmailValidated { get; set; }

        public string SecurityToken  {get; set; }

        public DateTime SecurityTokenExpires { get; set; }
    }
}
