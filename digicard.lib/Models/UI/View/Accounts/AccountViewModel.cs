﻿namespace digicard.Models.UI.View.Accounts
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class AccountViewModel
    {
        public int AccountId { get; set; }

        public int UserId { get; set; }
        
        public string Name { get; set; }

        public string BrainTreeSubId { get; set; }

        public string BrainTreeCustomerId { get; set; }

        public DateTime SubscriptionExpiry { get; set; }

        public DateTime SubscriptionCancelledAt { get; set; }
    }
}
