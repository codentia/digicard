﻿namespace digicard.Models.API.Request
{
    using System;
    using System.Collections.Generic;
    using digicard.Components.Shared.API;

    public interface IRequestModelValidateResult
    {
        IList<APIError> Errors { get; set; }
    }
}
