namespace digicard.Models.API.Request.Cards
{
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API.Accounts;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.Shared.API;

    public class GetCardsByAccountIdRequestModel : DigicardBaseAPIRequestModel, IRequestModel, IGetCardsByAccountIdRequest
    {
        public GetCardsByAccountIdRequestModel()
        {
            this.ErrorDictionary = AccountErrors.Instance;
            this.HttpMethod = APIHttpMethod.Post;
            this.BaseResource = BaseResource.Cards;
            this.SubResource = SubResource.None;
        }
        
        public string AccountId => GetParameterValue("AccountId");

        public IRequestModelValidateResult Result { get; private set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.AppendFormat("AccountId: {0}, ", this.AccountId);

            return builder.ToString();
        }

        public new async Task<bool> Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);

            GetCardsByAccountIdRequestModelValidateResult validateResult = new GetCardsByAccountIdRequestModelValidateResult();

            Account account = null;

            if (!string.IsNullOrEmpty(this.AccountId))
            {
                if (!int.TryParse(this.AccountId, out int accountIdValue) || accountIdValue < 1)
                {
                    AddValidationError(AccountErrors.AccountIdIsNotValid, this.AccountId);
                }
                else
                {
                    account = await Account.Get(accountIdValue);

                    if (account == null)
                    {
                        AddValidationError(AccountErrors.AccountDoesNotExist, this.AccountId);
                    }
                }
            }
            else
            {
                AddValidationError(AccountErrors.AccountIdIsRequired);
            }

            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.Account = account;
            }

            this.Result = validateResult;

            return Result.Errors.Count == 0;
        }
    }
}
