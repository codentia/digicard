namespace digicard.Models.API.Request.Cards
{
    public interface IGetCardsByAccountIdRequest : IRequest
    {
        string AccountId { get; }
    }
}
