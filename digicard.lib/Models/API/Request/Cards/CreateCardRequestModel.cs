namespace digicard.Models.API.Request.Cards
{
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.Shared.API;

    public class CreateCardRequestModel : DigicardBaseAPIRequestModel, IRequestModel, ICreateCardRequest
    {
        public CreateCardRequestModel()
        {
            this.ErrorDictionary = CardErrors.Instance;
            this.HttpMethod = APIHttpMethod.Post;
            this.BaseResource = BaseResource.Cards;
            this.SubResource = SubResource.None;
        }
        
        public string NameOnCard { get; set; }

        public string Description { get; set; }

        public string Mobile { get; set; }

        public string Landline  { get; set; }

        public string Email { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; }

        public string Instagram { get; set; }

        public string Domain { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string PostCode { get; set; }

        public string Country { get; set; }

        public IRequestModelValidateResult Result { get; private set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.AppendFormat("NameOnCard: {0}, ", this.NameOnCard);
            builder.AppendFormat("Description: {0}, ", this.Description);
            builder.AppendFormat("Mobile: {0}, ", this.Mobile);
            builder.AppendFormat("Landline: {0}, ", this.Landline);
            builder.AppendFormat("Email: {0}, ", this.Email);
            builder.AppendFormat("Facebook: {0}, ", this.Facebook);
            builder.AppendFormat("Twitter: {0}, ", this.Twitter);
            builder.AppendFormat("Instagram: {0}, ", this.Instagram);
            builder.AppendFormat("Domain: {0}, ", this.Domain);
            builder.AppendFormat("AddressLine1: {0}, ", this.AddressLine1);
            builder.AppendFormat("AddressLine2: {0}, ", this.AddressLine2);
            builder.AppendFormat("AddressLine3: {0}, ", this.AddressLine3);
            builder.AppendFormat("PostCode: {0}, ", this.PostCode);
            builder.AppendFormat("Country: {0}, ", this.Country);

            return builder.ToString();
        }

        public new async Task<bool> Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);

            CreateCardRequestModelValidateResult validateResult = new CreateCardRequestModelValidateResult();

            if (string.IsNullOrEmpty(this.NameOnCard))
            {
                AddValidationError(CardErrors.NameOnCardIsRequired);
            }

            if (string.IsNullOrEmpty(this.Description))
            {
                AddValidationError(CardErrors.DescriptionIsRequired);
            }

            if (string.IsNullOrEmpty(this.AddressLine1))
            {
                AddValidationError(CardErrors.AddressLine1IsRequired);
            }

            if (string.IsNullOrEmpty(this.PostCode))
            {
                AddValidationError(CardErrors.PostCodeIsRequired);
            }

            if (string.IsNullOrEmpty(this.Country))
            {
                AddValidationError(CardErrors.CountryIsRequired);
            }


            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.NameOnCard = this.NameOnCard;
                validateResult.Description = this.Description;
                validateResult.Mobile = this.Mobile;
                validateResult.Landline = this.Landline;
                validateResult.Email = this.Email;
                validateResult.Facebook = this.Facebook;
                validateResult.Twitter = this.Twitter;
                validateResult.Instagram = this.Instagram;
                validateResult.Domain = this.Domain;
                validateResult.AddressLine1 = this.AddressLine1;
                validateResult.AddressLine2 = this.AddressLine2;
                validateResult.AddressLine3 = this.AddressLine3;
                validateResult.PostCode = this.PostCode;
                validateResult.Country = this.Country;
            }

            this.Result = validateResult;

            return Result.Errors.Count == 0;
        }
    }
}
