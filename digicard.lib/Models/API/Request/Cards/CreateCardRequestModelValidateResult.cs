namespace digicard.Models.API.Request.Cards
{
    using System.Collections.Generic;
    using digicard.Components.Shared.API;

    public class CreateCardRequestModelValidateResult : IRequestModelValidateResult
    {
        public IList<APIError> Errors { get; set; }
        public string NameOnCard { get; set; }

        public string Description { get; set; }

        public string Mobile { get; set; }

        public string Landline  { get; set; }

        public string Email { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; }

        public string Instagram { get; set; }

        public string Domain { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string PostCode { get; set; }

        public string Country { get; set; }

    }
}
