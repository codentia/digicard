namespace digicard.Models.API.Request.Cards
{
    using System.Collections.Generic;
    using digicard.Components.Shared.API;
    using digicard.Components.API.Cards;

    public class GetCardBySlugRequestModelValidateResult : IRequestModelValidateResult
    {
        public IList<APIError> Errors { get; set; }
        public Card Card { get; set; }
    }
}
