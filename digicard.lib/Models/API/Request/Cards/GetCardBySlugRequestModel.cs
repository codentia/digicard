namespace digicard.Models.API.Request.Cards
{
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API.Cards;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.Shared.API;

    public class GetCardBySlugRequestModel : DigicardBaseAPIRequestModel, IRequestModel, IGetCardBySlugRequest
    {
        public GetCardBySlugRequestModel()
        {
            this.ErrorDictionary = CardErrors.Instance;
            this.HttpMethod = APIHttpMethod.Post;
            this.BaseResource = BaseResource.Cards;
            this.SubResource = SubResource.None;
        }
        
        public string Slug => GetParameterValue("Slug");

        public IRequestModelValidateResult Result { get; private set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.AppendFormat("Slug: {0}, ", this.Slug);

            return builder.ToString();
        }

        public new async Task<bool> Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);
            Card card = null;

            GetCardBySlugRequestModelValidateResult validateResult = new GetCardBySlugRequestModelValidateResult();

            if (string.IsNullOrEmpty(this.Slug))
            {
                AddValidationError(CardErrors.SlugIsRequired);
            }
            else
            {
                card = await Card.GetBySlug(this.Slug);

                if (card == null)
                {
                    AddValidationError(CardErrors.CardDoesNotExist);
                }
            }

            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.Card = card;
            }

            this.Result = validateResult;

            return Result.Errors.Count == 0;
        }
    }
}
