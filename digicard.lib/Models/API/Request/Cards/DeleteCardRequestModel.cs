namespace digicard.Models.API.Request.Cards
{
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API.Cards;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.Shared.API;
    using digicard.Models.API.Request.Cards;

    public class DeleteCardRequestModel : DigicardBaseAPIRequestModel, IRequestModel, IDeleteCardRequest
    {
        public DeleteCardRequestModel()
        {
            this.ErrorDictionary = CardErrors.Instance;
            this.HttpMethod = APIHttpMethod.Post;
            this.BaseResource = BaseResource.Accounts;
            this.SubResource = SubResource.None;
        }

        public string CardId => GetParameterValue("CardId");
    
        public IRequestModelValidateResult Result { get; private set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.Append($"{Name} - ");
            builder.AppendFormat("CardId: {0}, ", this.CardId);

            return builder.ToString();
        }

        public new async Task<bool> Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);

            DeleteCardRequestModelValidateResult validateResult = new DeleteCardRequestModelValidateResult();

            Card card = null;

            // and validate
            if (!string.IsNullOrEmpty(CardId))
            {
                if (!int.TryParse(CardId, out int cardIdValue) || cardIdValue < 1)
                {
                    AddValidationError(CardErrors.CardIdIsNotValid);
                }
                else
                {
                    card = await Card.Get(cardIdValue);

                    if (card == null)
                    {
                        AddValidationError(CardErrors.CardDoesNotExist);
                    }
                }
            }
            else
            {
                AddValidationError(CardErrors.CardIdIsRequired);
            }

            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.Card = card;
               
            }

            this.Result = validateResult;

            return Result.Errors.Count == 0;
        }


    }
}
