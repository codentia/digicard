namespace digicard.Models.API.Request.Cards
{
    using System.Collections.Generic;
    using digicard.Components.API.Cards;
    using digicard.Components.Shared.API;

    public class DeleteCardRequestModelValidateResult : IRequestModelValidateResult
    {
        public IList<APIError> Errors { get; set; }

        public Card Card { get; set; }
    }

}
