namespace digicard.Models.API.Request.Cards
{
    public interface IGetCardBySlugRequest : IRequest
    {

        string Slug { get; }
    }
}
