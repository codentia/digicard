namespace digicard.Models.API.Request.Cards
{
    using System.Collections.Generic;
    using digicard.Components.API.Accounts;
    using digicard.Components.Shared.API;

    public class GetCardsByAccountIdRequestModelValidateResult : IRequestModelValidateResult
    {
        public IList<APIError> Errors { get; set; }

        public Account Account { get; set; }
    }
}
