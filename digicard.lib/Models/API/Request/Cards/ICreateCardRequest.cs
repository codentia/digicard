namespace digicard.Models.API.Request.Cards
{
    public interface ICreateCardRequest : IRequest
    {

        string NameOnCard { get; set; }

        string Description { get; set; }

        string Mobile { get; set; }

        string Landline  { get; set; }

        string Email { get; set; }

        string Facebook { get; set; }

        string Twitter { get; set; }

        string Instagram { get; set; }

        string Domain { get; set; }

        string AddressLine1 { get; set; }

        string AddressLine2 { get; set; }

        string AddressLine3 { get; set; }

        string PostCode { get; set; }

        string Country { get; set; }
    }
}
