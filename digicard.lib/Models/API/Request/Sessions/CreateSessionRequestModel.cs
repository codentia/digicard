﻿namespace digicard.Models.API.Request.Sessions
{
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API;
    using digicard.Components.Shared.API; 
    using digicard.Components.API.ErrorCodes;
    using digicard.Models.API.Request.Sessions;

    public class CreateSessionRequestModel : DigicardBaseAPIRequestModel, IRequestModel, ICreateSessionRequest
    {
        public CreateSessionRequestModel()
        {
            this.ErrorDictionary = AuthenticationErrors.Instance;
            this.HttpMethod = APIHttpMethod.Post;
            this.BaseResource = BaseResource.System;
            this.SubResource = SubResource.Authentication;
        }

        public string Username { get; set; }

        public string Password { get; set; }

        public string IPAddress { get; set; }

        public IRequestModelValidateResult Result { get; private set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.Append($"{Name} - ");
            builder.AppendFormat("Username: {0}, ", this.Username);
            builder.AppendFormat("Password: {0}, ", "**********");
            builder.AppendFormat("IPAddress: {0} ", this.IPAddress);
            return builder.ToString();
        }

        public new async Task<bool> Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);

            CreateSessionRequestModelValidateResult validateResult = new CreateSessionRequestModelValidateResult();

            // and validate
            if (string.IsNullOrEmpty(Username))
            {
                AddValidationError(AuthenticationErrors.UsernameIsRequired);
            }

            if (string.IsNullOrEmpty(Password))
            {
                AddValidationError(AuthenticationErrors.PasswordIsRequired);
            }

            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.Username = this.Username;
                validateResult.Password = this.Password;
                validateResult.IPAddress = this.IPAddress;
            }

            this.Result = validateResult;

            return Result.Errors.Count == 0;
        }
    }
}
