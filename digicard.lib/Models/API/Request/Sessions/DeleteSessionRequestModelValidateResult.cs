﻿namespace digicard.Models.API.Request.Sessions
{
    using System.Collections.Generic;
    using digicard.Components.Shared.API;

    public class DeleteSessionRequestModelValidateResult : IRequestModelValidateResult
    {
        public IList<APIError> Errors { get; set; }

        public string Username { get; set; }
    }
}
