﻿namespace digicard.Models.API.Request.Sessions
{
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API;
    using digicard.Components.Shared.API;
    using digicard.Components.API.ErrorCodes;

    public class RetrieveSessionRequestModel : DigicardBaseAPIRequestModel, IRequestModel
    {
        public RetrieveSessionRequestModel()
        {
            this.ErrorDictionary = AuthenticationErrors.Instance;
            this.HttpMethod = APIHttpMethod.Get;
            this.BaseResource = BaseResource.System;
            this.SubResource = SubResource.Authentication;
        }

        public IRequestModelValidateResult Result { get; private set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.Append($"{Name} - ");
            return builder.ToString();
        }

        public new async Task<bool> Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);

            RetrieveSessionRequestModelValidateResult validateResult = new RetrieveSessionRequestModelValidateResult();

            string username = GetParameterValue("Username");

            // and validate
            if (string.IsNullOrEmpty(username))
            {
                AddValidationError(AuthenticationErrors.UsernameIsRequired);
            }

            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.Username = username;
            }

            this.Result = validateResult;

            return Result.Errors.Count == 0;
        }
    }
}
