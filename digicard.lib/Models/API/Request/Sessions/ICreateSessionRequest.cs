﻿namespace digicard.Models.API.Request.Sessions
{
    public interface ICreateSessionRequest : IRequest
    {
        string Username { get; set; }

        string Password { get; set; }

        string IPAddress { get; set; }
    }
}
