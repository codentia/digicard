namespace digicard.Models.API.Request.Users
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API;
    using digicard.Components.API.Authentication;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.Shared.API;
    using digicard.Components.API.Users;
    using digicard.Models.API.Request.Users;

    public class FinishPasswordResetRequestModel : DigicardBaseAPIRequestModel, IRequestModel
    {
        public FinishPasswordResetRequestModel()
        {
            this.ErrorDictionary = UserErrors.Instance;
            this.HttpMethod = APIHttpMethod.Post;
            this.BaseResource = BaseResource.Users;
            this.SubResource = SubResource.Authentication;
        }

        public string EmailAddress { get; set; }

        public string Token { get; set; }

        public string Password { get; set; }

        public string PasswordHash { get; set; }

        public User User { get; set; }

        public IRequestModelValidateResult Result { get; set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.Append($"{Name} - ");
            builder.AppendFormat("EmailAddress: {0}", EmailAddress);
            return builder.ToString();
        }

        public new async Task Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);

            FinishPasswordResetRequestModelValidateResult validateResult = new FinishPasswordResetRequestModelValidateResult();

            if (string.IsNullOrEmpty(Token))
            {
                AddValidationError(UserErrors.TokenIsRequired);
            }

            if (string.IsNullOrEmpty(Password))
            {
                AddValidationError(UserErrors.PasswordIsRequired);
            }
            else
            {
                PasswordHash = PasswordManager.GenerateSaltedHash(this.Password, PasswordManager.GenerateSaltValue());
            }

            if (Errors.Count == 0)
            {
                User = await User.GetByToken(Token);

                if (User == null)
                {
                    AddValidationError(UserErrors.TokenIsInvalid);
                }
                else if (User.SecurityTokenExpires <= DateTime.Now)
                {
                    AddValidationError(UserErrors.TokenIsExpired);
                }
                else if (PasswordManager.ComparePasswordHash(User.PasswordHash, this.Password))
                {
                    AddValidationError(UserErrors.PasswordCantBeSameAsOld);
                }
                else if (this.Password.Length <= 8)
                {
                    AddValidationError(UserErrors.PasswordDoesNotMinimumCharacterLength);
                }
            }

            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.Token = Token;
                validateResult.PasswordHash = PasswordHash;
                validateResult.User = User;
            }

            this.Result = validateResult;
        }
    }
}
