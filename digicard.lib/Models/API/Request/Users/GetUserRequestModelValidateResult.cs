﻿namespace digicard.Models.API.Request.Users
{
    using System.Collections.Generic;
    using digicard.Components.API;
    using digicard.Components.API.Users;
    using digicard.Components.Shared.API;

    public class GetUserRequestModelValidateResult : IRequestModelValidateResult
    {
        public IList<APIError> Errors { get; set; }

        public User User { get; set; }
    }
}
