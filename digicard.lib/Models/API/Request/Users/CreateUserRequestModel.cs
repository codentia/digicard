﻿namespace digicard.Models.API.Request.Users
{
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API;
    using digicard.Components.Shared.API; 
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.API.Users;
    using digicard.Models.API.Request.Users;

    public class CreateUserRequestModel : DigicardBaseAPIRequestModel, IRequestModel, ICreateUserRequest
    {
        public CreateUserRequestModel()
        {
            this.ErrorDictionary = UserErrors.Instance;
            this.HttpMethod = APIHttpMethod.Post;
            this.BaseResource = BaseResource.Users;
            this.SubResource = SubResource.None;
        }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public IRequestModelValidateResult Result { get; private set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.Append($"{Name} - ");
            builder.AppendFormat("EmailAddress: {0}, ", this.EmailAddress);
            builder.AppendFormat("Password: {0}, ", "**********");
            return builder.ToString();
        }

        public new async Task<bool> Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);

            CreateUserRequestModelValidateResult validateResult = new CreateUserRequestModelValidateResult();

            // and validate
            if (string.IsNullOrEmpty(EmailAddress))
            {
                AddValidationError(UserErrors.EmailAddressIsRequired);
            }
            else
            {
                if (await User.Get(EmailAddress) != null)
                {
                    AddValidationError(UserErrors.UserAlreadyExists);
                }
            }

            if (string.IsNullOrEmpty(Password))
            {
                AddValidationError(UserErrors.PasswordIsRequired);
            }

            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.EmailAddress = this.EmailAddress;
                validateResult.Password = this.Password;
            }

            this.Result = validateResult;

            return Result.Errors.Count == 0;
        }
    }
}
