﻿namespace digicard.Models.API.Request.Users
{
    using System.Collections.Generic;
    using digicard.Components.Shared.API;
    using digicard.Components.API.Users;

    public class FinishPasswordResetRequestModelValidateResult : IRequestModelValidateResult
    {
        public IList<APIError> Errors { get; set; }

        public User User { get; set; }

        public string Token { get; set; }

        public string PasswordHash { get; set; }

        public string EmailAddress { get; set; }
    }
}
