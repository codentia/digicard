﻿namespace digicard.Models.API.Request.Users
{
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API;
    using digicard.Components.Shared.API;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.API.Users;
    using digicard.Models.API.Request.Users;

    public class GetUserRequestModel : DigicardBaseAPIRequestModel, IRequestModel
    {
        public GetUserRequestModel()
        {
            this.ErrorDictionary = UserErrors.Instance;
            this.HttpMethod = APIHttpMethod.Get;
            this.BaseResource = BaseResource.Users;
            this.SubResource = SubResource.None;
        }

        public string UserId => GetParameterValue("UserId");

        public IRequestModelValidateResult Result { get; private set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.Append($"{Name} - ");
            builder.AppendFormat("UserId: {0}", this.UserId);
            return builder.ToString();
        }

        public new async Task<bool> Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);

            GetUserRequestModelValidateResult validateResult = new GetUserRequestModelValidateResult();

            User user = null;

            // and validate
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!int.TryParse(UserId, out int userIdValue) || userIdValue < 1)
                {
                    AddValidationError(UserErrors.UserIdIsNotValid);
                }
                else
                {
                    user = await User.Get(userIdValue);

                    if (user == null)
                    {
                        AddValidationError(UserErrors.UserDoesNotExist);
                    }
                }
            }
            else
            {
                AddValidationError(UserErrors.UserIdIsRequired);
            }

            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.User = user;
            }

            this.Result = validateResult;

            return Result.Errors.Count == 0;
        }
    }
}
