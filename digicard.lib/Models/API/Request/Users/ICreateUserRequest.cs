﻿namespace digicard.Models.API.Request.Users
{
    public interface ICreateUserRequest : IRequest
    {
        string EmailAddress { get; set; }

        string Password { get; set; }
    }
}
