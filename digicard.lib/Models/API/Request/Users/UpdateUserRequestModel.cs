﻿namespace digicard.Models.API.Request.Users
{
    public class UpdateUserRequestModel : IUpdateUserRequest
    {
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
