﻿namespace digicard.Models.API.Request.Users
{
    using System.Collections.Generic;
    using digicard.Components.API;
    using digicard.Components.Shared.API;

    public class CreateUserRequestModelValidateResult : IRequestModelValidateResult
    {
        public IList<APIError> Errors { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }
    }
}
