﻿namespace digicard.Models.API.Request.Users
{
    public interface IUpdateUserRequest : IRequest
    {
        string Password { get; set; }

        string ConfirmPassword { get; set; }
    }
}
