namespace digicard.Models.API.Request.Users
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API;
    using digicard.Components.Shared.API;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.API.Users;
    using digicard.Models.API.Request.Users;

    public class StartPasswordResetRequestModel : DigicardBaseAPIRequestModel, IRequestModel
    {
        public StartPasswordResetRequestModel()
        {
            this.ErrorDictionary = UserErrors.Instance;
            this.HttpMethod = APIHttpMethod.Post;
            this.BaseResource = BaseResource.Users;
            this.SubResource = SubResource.Authentication;
        }

        public string EmailAddress { get; set; }

        public IRequestModelValidateResult Result { get; set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.Append($"{Name} - ");
            builder.AppendFormat("EmailAddress: {0}", EmailAddress);
            return builder.ToString();
        }

        public new async Task Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);

            StartPasswordResetRequestModelValidateResult validateResult = new StartPasswordResetRequestModelValidateResult();
            User user = null;

            if (string.IsNullOrEmpty(EmailAddress))
            {
                AddValidationError(UserErrors.EmailAddressIsRequired);
            }
            else
            {
                user = await User.Get(EmailAddress);
            }

            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.EmailAddress = EmailAddress;
                validateResult.User = user;
            }

            this.Result = validateResult;
        }
    }
}
