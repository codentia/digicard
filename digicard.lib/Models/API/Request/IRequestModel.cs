﻿namespace digicard.Models.API.Request
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IRequestModel
    {
        IRequestModelValidateResult Result { get; }

        Task Validate(DigicardBaseAPIRequestModel parent = null);

        string ToParamList();
    }
}
