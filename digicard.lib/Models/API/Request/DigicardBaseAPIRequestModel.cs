﻿namespace digicard.Models.API.Request
{
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API;
    using digicard.Components.Shared.API;
    using digicard.Components.API.Users;
    using digicard.Models.API;

    public abstract class DigicardBaseAPIRequestModel
    {
        protected DigicardBaseAPIRequestModel()
        {
            this.Errors = new List<APIError>();
            this.UrlParameters = new Dictionary<string, string>();
        }

        public string Name => this.GetType().Name;

        //public User CurrentUser { get; set; }

        public APIHttpMethod HttpMethod { get; set; }

        public BaseResource BaseResource { get; set; }

        public SubResource SubResource { get; set; }

        public Dictionary<string, string> UrlParameters { get; set; }

        protected Dictionary<int, string> ErrorDictionary { get; set; }

        protected List<APIError> Errors { get; set; }

        public override string ToString()
        {
            var urlParamBuilder = new StringBuilder();

            int paramCount = 1;
            foreach (string s in UrlParameters.Keys)
            {
                urlParamBuilder.AppendFormat("{0}{1}", s, string.IsNullOrEmpty(UrlParameters[s]) ? " is null or empty" : $": {UrlParameters[s]}");
                if (paramCount != UrlParameters.Keys.Count)
                {
                    urlParamBuilder.Append(", ");
                }

                paramCount++;
            }

            string requestModelString = ((IRequestModel)this).ToParamList();

            string separator = string.Empty;
            if (urlParamBuilder.Length > 0 && requestModelString.Length > 0)
            {
                separator = ", ";
            }

            return $"{urlParamBuilder.ToString()}{separator}{requestModelString}";
        }

        //// public abstract bool SupplementarySecurity(Dictionary<string, string> urlParams, Account requestor);

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task Validate(DigicardBaseAPIRequestModel parent = null)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            if (parent != null)
            {
                this.HttpMethod = parent.HttpMethod;
                this.BaseResource = parent.BaseResource;
                this.SubResource = this.SubResource;
            }
        }

        protected void AddValidationError(int errorId, string errorText = "")
        {
            this.Errors.Add(APIError.GetValidationAPIError(baseResource: BaseResource, subResource: SubResource, errorDictionary: ErrorDictionary, httpMethod: HttpMethod, errorId: errorId, errorText: errorText));
        }

        /*
        protected void AddPermissionsError()
        {
            this.Errors.Add(APIError.GetPermissionsError(this));
        }
        */

        // protected bool ParameterIsTrue(string key)
        // {
        //     bool retVal = false;
        //     if (UrlParameters != null && UrlParameters.ContainsKey(key))
        //     {
        //         retVal = bool.TryParse(UrlParameters[key], out bool value) && value;
        //     }

        //     return retVal;
        // }

        protected string GetParameterValue(string key)
        {
            return UrlParameters.ContainsKey(key) ? UrlParameters[key] : string.Empty;
        }
    }
}
