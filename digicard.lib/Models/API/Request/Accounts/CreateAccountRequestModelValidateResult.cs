﻿namespace digicard.Models.API.Request.Accounts
{
    using System.Collections.Generic;
    using digicard.Components.Shared.API;

    public class CreateAccountRequestModelValidateResult : IRequestModelValidateResult
    {
        public IList<APIError> Errors { get; set; }

        public string Name { get; set; }
    }
}
