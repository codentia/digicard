﻿namespace digicard.Models.API.Request.Accounts
{
    using System.Text;
    using System.Threading.Tasks;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.Shared.API;
    using digicard.Models.API.Request.Accounts;

    public class CreateAccountRequestModel : DigicardBaseAPIRequestModel, IRequestModel, ICreateAccountRequest
    {
        public CreateAccountRequestModel()
        {
            this.ErrorDictionary = AccountErrors.Instance;
            this.HttpMethod = APIHttpMethod.Post;
            this.BaseResource = BaseResource.Accounts;
            this.SubResource = SubResource.None;
        }

        public string Name { get; set; }
                                                
        public IRequestModelValidateResult Result { get; private set; }

        public string ToParamList()
        {
            var builder = new StringBuilder();
            builder.Append($"{Name} - ");
            builder.AppendFormat("Name: {0}, ", this.Name);

            return builder.ToString();
        }

        public new async Task<bool> Validate(DigicardBaseAPIRequestModel parent = null)
        {
            await base.Validate(parent);

            CreateAccountRequestModelValidateResult validateResult = new CreateAccountRequestModelValidateResult();

            if (string.IsNullOrEmpty(Name))
            {
                AddValidationError(AccountErrors.NameIsRequired);
            }

            validateResult.Errors = Errors;

            if (Errors.Count == 0)
            {
                validateResult.Name = this.Name;
            }

            this.Result = validateResult;

            return Result.Errors.Count == 0;
        }
    }
}
