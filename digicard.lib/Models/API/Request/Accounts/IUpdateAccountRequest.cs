﻿namespace digicard.Models.API.Request.Accounts
{
    using digicard.Models.API.Request;

    public interface IUpdateAccountRequest : IRequest
    {
    }
}
