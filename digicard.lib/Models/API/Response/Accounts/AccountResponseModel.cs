﻿namespace digicard.Models.API.Response.Accounts
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using digicard;

    public class AccountResponseModel : IAccountResponse
    {
        public string AccountId { get; set; }

        public string UserId { get; set; }
        
        public string Name { get; set; }

        public string BrainTreeSubId { get; set; }

        public string BrainTreeCustomerId { get; set; }

        public string SubscriptionExpiry { get; set; }

        public string SubscriptionCancelledAt { get; set; }
    }
}
