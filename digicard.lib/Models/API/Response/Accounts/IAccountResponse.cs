﻿namespace digicard.Models.API.Response.Accounts
{
    using System;

    interface IAccountResponse
    {
         string AccountId { get; set; }

         string UserId { get; set; }
        
         string Name { get; set; }

         string BrainTreeSubId { get; set; }

         string BrainTreeCustomerId { get; set; }

         string SubscriptionExpiry { get; set; }

         string SubscriptionCancelledAt { get; set; }
    }
}
