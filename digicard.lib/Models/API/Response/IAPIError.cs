﻿namespace digicard.Models.API.Response
{
    public interface IAPIError
    {
        string Code { get; set; }

        string Detail { get; }

        string Diagnostic { get; set; }

        string FriendlyMessage { get; set; }

        bool IsInternalError { get; set; }
    }
}
