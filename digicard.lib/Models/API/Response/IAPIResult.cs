﻿namespace digicard.Models.API.Response
{
    using System.Collections.Generic;

    public interface IAPIResult
    {
        bool Success { get; }

        List<IAPIError> Errors { get; }

        dynamic Data { get; set; }
    }
}
