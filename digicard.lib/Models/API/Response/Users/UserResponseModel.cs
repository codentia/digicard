﻿namespace digicard.Models.API.Response.Users
{
    using Newtonsoft.Json;
    using digicard;

    public class UserResponseModel : IUserResponse
    {
        public string UserId { get; set; }

        public string EmailAddress { get; set; }

        public string IsAuthenticated { get; set; }

        public string SecurityToken { get; set; }

        public string SecurityTokenExpires { get; set; }

        public string IsEmailValidated { get; set; }
    }
}
