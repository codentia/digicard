﻿namespace digicard.Models.API.Response.Users
{
    using System.Collections.Generic;
    using digicard.Models.API.Response.Accounts;

    public interface IUserResponse
    {
        string UserId { get; set; }

        string EmailAddress { get; set; }

        string IsAuthenticated { get; set; }

        string SecurityToken { get; set; }

        string SecurityTokenExpires { get; set; }

        string IsEmailValidated { get; set; }
    }
}
