﻿namespace digicard.Models.API.Response.Users
{
    using System;
    using Newtonsoft.Json;
    using digicard.Models.API.Response.Users;

    public class LoginResponseModel : ILoginResponse
    {
        [JsonConverter(typeof(ConcreteConverter<UserResponseModel>))]
        public IUserResponse User { get; set; }
    }
}
