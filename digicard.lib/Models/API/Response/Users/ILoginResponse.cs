﻿namespace digicard.Models.API.Response.Users
{
    public interface ILoginResponse
    {
        IUserResponse User { get; set; }
    }
}
