CREATE TABLE messagetemplate
(
    MessageTemplateId       INT NOT NULL AUTO_INCREMENT,
    MessageTemplateCode     VARCHAR(100) NOT NULL,
    IsHtml                  TINYINT(1) NOT NULL DEFAULT 1,
    IsAdmin                 TINYINT(1) NOT NULL DEFAULT 0,
    ParentTemplateId        INT NULL,
    Body                    MEDIUMTEXT NOT NULL,
    Subject                 VARCHAR(255) NULL,
    
    PRIMARY KEY(MessageTemplateId),
    FOREIGN KEY (ParentTemplateId) REFERENCES messagetemplate (MessageTemplateId)
) ENGINE=Innodb;

CREATE UNIQUE INDEX UX_MessageTemplate ON messagetemplate (MessageTemplateCode);
