CREATE TABLE auditlog
(
    AuditLogId              INT AUTO_INCREMENT NOT NULL,
    AccountId               INT NULL,
    UserId                  INT NULL,
    ObjectId                INT NOT NULL,
    ObjectType              VARCHAR(50) NOT NULL,
    OperationType           VARCHAR(10) NOT NULL,
    Detail                  MEDIUMTEXT NOT NULL,
    LogStamp                DATETIME NOT NULL,
    
    CreatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (AuditLogId),
    FOREIGN KEY (AccountId) REFERENCES account (AccountId),
    FOREIGN KEY (UserId) REFERENCES user (UserId)
) ENGINE=innodb;
