CREATE TABLE messagehistory
(
    MessageHistoryId        INT NOT NULL AUTO_INCREMENT,
    MessageTemplateId       INT NOT NULL,
    AccountId               INT NULL,
    ToUserId                INT NULL,
    EmailAddress            VARCHAR(250) NOT NULL,
    Body                    MEDIUMTEXT NOT NULL,
    Subject                 VARCHAR(255) NULL,

    CreatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (MessageHistoryId),
    FOREIGN KEY (MessageTemplateId) REFERENCES messagetemplate (MessageTemplateId),
    FOREIGN KEY (AccountId) REFERENCES account (AccountId),
    FOREIGN KEY (ToUserId) REFERENCES user (UserId),
) ENGINE=innodb;
