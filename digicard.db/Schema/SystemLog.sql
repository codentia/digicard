CREATE TABLE systemlog
(
    SystemLogId             INT AUTO_INCREMENT NOT NULL,
    AccountId               INT NULL,
    UserId                  INT NULL,
    Component               VARCHAR(150) NOT NULL,
    Method                  VARCHAR(150) NOT NULL,
    Severity                INT NOT NULL DEFAULT 1,
    Detail                  MEDIUMTEXT NOT NULL,
    StackTrace              MEDIUMTEXT NOT NULL,
    LoggedAt                DATETIME NOT NULL,

    CreatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (SystemLogId),
    FOREIGN KEY (AccountId) REFERENCES account (AccountId),
    FOREIGN KEY (UserId) REFERENCES user (UserId)
) ENGINE=innodb;
