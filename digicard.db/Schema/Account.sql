CREATE TABLE account
(
    AccountId               INT NOT NULL AUTO_INCREMENT,
    UserId                  INT NOT NULL,
    AccountName             VARCHAR(50) NOT NULL,
    BraintreeCustromerId    VARCHAR(50) NULL,
    BraintreeSubscriptionId VARCHAR(64) NULL,
    SubscriptionExpiry      DATETIME NOT NULL DEFAULT NOW() - INTERVAL 1 WEEK,
    SubscriptionCancelledAt DATETIME NULL,

    CreatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    PRIMARY KEY (AccountId),
    FOREIGN KEY (UserId) REFERENCES user (UserId) 
) ENGINE=innodb;
