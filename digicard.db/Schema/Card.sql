CREATE TABLE card
(
    CardId        INT NOT NULL AUTO_INCREMENT,
    UserId        INT NOT NULL,
    NameOnCard    VARCHAR(100) NOT NULL,
    Slug          VARCHAR(100) NOT NULL,
    Description   VARCHAR(255) NOT NULL,
    Mobile        VARCHAR(30) NULL,
    Landline      VARCHAR(30) NULL,
    Email         VARCHAR(150) NULL,
    Facebook      VARCHAR(255) NULL,
    Twitter       VARCHAR(255) NULL,
    Instagram     VARCHAR(255) NULL,
    Logo          CHAR(38) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
    LogoExtension VARCHAR(10) NULL,
    Domain        VARCHAR(255) NULL,
    AddressLine1  VARCHAR(100) NOT NULL,
    AddressLine2  VARCHAR(100),
    AddressLine3  VARCHAR(100),
    PostCode      VARCHAR(10) NOT NULL,
    Country       VARCHAR(30) NOT NULL,
    IsPublished   TINYINT(1) NOT NULL DEFAULT 0,


    CreatedAt     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    UNIQUE (Slug),
    
    PRIMARY KEY (CardId),
    FOREIGN KEY (UserId) REFERENCES user (UserId) 
) ENGINE=innodb;
