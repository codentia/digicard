use mysql;
drop database if exists digicard;
CREATE DATABASE if not exists digicard;
USE digicard;


CREATE TABLE user
(
    UserId                INT NOT NULL AUTO_INCREMENT,
    EmailAddress          VARCHAR(150) NOT NULL,
    PasswordHash          VARCHAR(512) NOT NULL,
    ExternalToken         VARCHAR(36) NOT NULL,
    ExternalTokenExpiry   DATETIME NULL,
    EmailValidatedAt      DATETIME NULL,

    CreatedAt             TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt             TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    PRIMARY KEY (UserId)
) ENGINE=innodb;


CREATE TABLE account
(
    AccountId               INT NOT NULL AUTO_INCREMENT,
    UserId                  INT NOT NULL,
    AccountName             VARCHAR(50) NOT NULL,
    BraintreeCustromerId    VARCHAR(50) NULL,
    BraintreeSubscriptionId VARCHAR(64) NULL,
    SubscriptionExpiry      DATETIME NOT NULL DEFAULT (NOW() - INTERVAL 1 WEEK),
    SubscriptionCancelledAt DATETIME NULL,

    CreatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    PRIMARY KEY (AccountId),
    FOREIGN KEY (UserId) REFERENCES user (UserId) 
) ENGINE=innodb;


CREATE TABLE card
(
    CardId        INT NOT NULL AUTO_INCREMENT,
    UserId        INT NOT NULL,
    NameOnCard    VARCHAR(100) NOT NULL,
    Slug          VARCHAR(100) NOT NULL,
    Description   VARCHAR(255) NOT NULL,
    Mobile        VARCHAR(30) NULL,
    Landline      VARCHAR(30) NULL,
    Email         VARCHAR(150) NULL,
    Facebook      VARCHAR(255) NULL,
    Twitter       VARCHAR(255) NULL,
    Instagram     VARCHAR(255) NULL,
    Logo          CHAR(38) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
    LogoExtension VARCHAR(10) NULL,
    Domain        VARCHAR(255) NULL,
    AddressLine1  VARCHAR(100) NOT NULL,
    AddressLine2  VARCHAR(100),
    AddressLine3  VARCHAR(100),
    PostCode      VARCHAR(10) NOT NULL,
    Country       VARCHAR(30) NOT NULL,
    IsPublished   TINYINT(1) NOT NULL DEFAULT 0,


    CreatedAt     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    UNIQUE (Slug),
    
    PRIMARY KEY (CardId),
    FOREIGN KEY (UserId) REFERENCES user (UserId) 
) ENGINE=innodb;


CREATE TABLE openingtime
(
    OpeningTimeId INT NOT NULL AUTO_INCREMENT,
    CardId        INT NOT NULL,
    DayOfWeek     VARCHAR(10) NOT NULL,
    IsOpen        TINYINT(1) NOT NULL DEFAULT 0,
    Start         VARCHAR(5) NOT NULL,
    End           VARCHAR(5) NOT NULL,

    CreatedAt     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    
    PRIMARY KEY (OpeningTimeId),
    FOREIGN KEY (CardId) REFERENCES card (CardId) 
) ENGINE=innodb;


CREATE TABLE auditlog
(
    AuditLogId              INT AUTO_INCREMENT NOT NULL,
    AccountId               INT NULL,
    UserId                  INT NULL,
    ObjectId                INT NOT NULL,
    ObjectType              VARCHAR(50) NOT NULL,
    OperationType           VARCHAR(10) NOT NULL,
    Detail                  MEDIUMTEXT NOT NULL,
    LogStamp                DATETIME NOT NULL,
    
    CreatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (AuditLogId),
    FOREIGN KEY (AccountId) REFERENCES account (AccountId),
    FOREIGN KEY (UserId) REFERENCES user (UserId)
) ENGINE=innodb;


CREATE TABLE systemlog
(
    SystemLogId             INT AUTO_INCREMENT NOT NULL,
    AccountId               INT NULL,
    UserId                  INT NULL,
    Component               VARCHAR(150) NOT NULL,
    Method                  VARCHAR(150) NOT NULL,
    Severity                INT NOT NULL DEFAULT 1,
    Detail                  MEDIUMTEXT NOT NULL,
    StackTrace              MEDIUMTEXT NOT NULL,
    LoggedAt                DATETIME NOT NULL,

    CreatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (SystemLogId),
    FOREIGN KEY (AccountId) REFERENCES account (AccountId),
    FOREIGN KEY (UserId) REFERENCES user (UserId)
) ENGINE=innodb;


CREATE TABLE messagetemplate
(
    MessageTemplateId       INT NOT NULL AUTO_INCREMENT,
    MessageTemplateCode     VARCHAR(100) NOT NULL,
    IsHtml                  TINYINT(1) NOT NULL DEFAULT 1,
    IsAdmin                 TINYINT(1) NOT NULL DEFAULT 0,
    ParentTemplateId        INT NULL,
    Body                    MEDIUMTEXT NOT NULL,
    Subject                 VARCHAR(255) NULL,
    
    PRIMARY KEY(MessageTemplateId),
    FOREIGN KEY (ParentTemplateId) REFERENCES messagetemplate (MessageTemplateId)
) ENGINE=Innodb;

CREATE UNIQUE INDEX UX_MessageTemplate ON messagetemplate (MessageTemplateCode);


CREATE TABLE messagehistory
(
    MessageHistoryId        INT NOT NULL AUTO_INCREMENT,
    MessageTemplateId       INT NOT NULL,
    AccountId               INT NULL,
    ToUserId                INT NULL,
    EmailAddress            VARCHAR(250) NOT NULL,
    Body                    MEDIUMTEXT NOT NULL,
    Subject                 VARCHAR(255) NULL,

    CreatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UpdatedAt               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (MessageHistoryId),
    FOREIGN KEY (MessageTemplateId) REFERENCES messagetemplate (MessageTemplateId),
    FOREIGN KEY (AccountId) REFERENCES account (AccountId),
    FOREIGN KEY (ToUserId) REFERENCES user (UserId)
) ENGINE=innodb;

