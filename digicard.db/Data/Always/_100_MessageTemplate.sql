-- master template
INSERT INTO messagetemplate (MessageTemplateId, MessageTemplateCode, IsHtml, IsAdmin, ParentTemplateId, Body, Subject)
VALUES (1, 'MASTER_EMAIL', 1, 0, NULL, '<html> <head> <style> @import url("https://fonts.googleapis.com/css?family=Artifika|Lato");  h1, h2, h3, h4, h5, h6 { font-family: "Artifika", serif; } p { font-family: "Lato", sans-serif; }  html, body { width: 100%; height: 100%; margin: 0; padding: 0; background: #463a3c; }  table.main { width: 100%; max-width: 600px; background-color: white; margin: 0 auto; } .header th { border-bottom: solid 2px #b9094e; } table > table th { text-align: left; } </style> </head>  <body> <table cellpadding="20" class="main"> <thead class="header"> <tr> <th align="center"> <img src="[[LOGO_URL]]" width="150px" alt="Logo" /> </th> </tr> </thead> <tbody> <tr> <td> [[MAILTEXT]] </td> </tr> </tbody> </table> </body> </html>', NULL);

-- child templates
INSERT INTO messagetemplate (MessageTemplateId, MessageTemplateCode, IsHtml, IsAdmin, ParentTemplateId, Body, Subject)
VALUES 
(2, 'SYS_START_PASSWORD_RESET', 1, 0, 1, '<p>Someone has requested to reset the password for your account. If this was not you, please ignore this message. Otherwise, please click this link: <a href="[[SITE_PREFIX]]/Authentication/FinishPasswordReset/[[USER_TOKEN]]">[[SITE_PREFIX]]/Authentication/FinishPasswordReset/[[USER_TOKEN]]</a></p>', 'Reset your password'),
(3, 'SYS_FINISH_PASSWORD_RESET', 1, 0, 1, '<p>Your password has been reset successfully.</p>', 'Password Reset Successfully'),
(4, 'SYS_CONFIRMEMAIL', 1, 0, 1, 'Someone has recently registered your email address on our system. If this was you, please click the following link, otherwise you can ignore this message: <a href="[[SITE_PREFIX]]/Profile/ConfirmEmail/[[USER_TOKEN]]?email=[[USER_EMAIL]]">[[SITE_PREFIX]]/Profile/ConfirmEmail/[[USER_TOKEN]]?email=[[USER_EMAIL]]</a>.', 'Confirm your Email Address');
