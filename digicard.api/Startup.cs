using System;
namespace digicard.api
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.DataProtection;
    using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.HttpsPolicy;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.StackExchangeRedis;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Microsoft.IdentityModel.Tokens;
    using Microsoft.OpenApi.Models;
    using digicard;
    using digicard.Components.API.Accounts;
    using digicard.Components.API.Cards;
    using digicard.Components.API.Messaging;
    using digicard.Components.API.Users;
    using digicard.Components.Cron;
    using digicard.Components.Shared.Logging;
    using StackExchange.Redis;

    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: false)
            .AddJsonFile($"secrets/appsettings.{env.EnvironmentName}.json", true, false)
            .AddEnvironmentVariables();

            // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<AppSettings>();
            }

            this.Configuration = builder.Build();
            this.Environment = env;
        }
        
        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Environment { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            AppSettings.Current = Configuration.GetSection("AppSettings").Get<AppSettings>();
            services.Configure<AppSettings>(this.Configuration.GetSection("AppSettings"));
            services.AddOptions();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "digicard.api", Version = "v1" });
            });

            services.AddAuthentication().AddJwtBearer(config =>
            {
                config.RequireHttpsMetadata = false;
                config.SaveToken = true;
                config.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidIssuer = this.Configuration["AppSettings:JWTIssuer"],
                    ValidAudience = this.Configuration["AppSettings:JWTIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.Configuration["AppSettings:JWTKey"])),
                    ClockSkew = TimeSpan.Zero,
                };
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("IsLoggedIn", policy =>
                {
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                    policy.RequireAuthenticatedUser();
                });
            });

            // register context services
            services.AddTransient<IAccountsDbContext, AccountsDbContext>();
            services.AddTransient<IEmailDbContext, EmailDbContext>();
            services.AddTransient<ILoggingDbContext, LoggingDbContext>();
            services.AddTransient<IUsersDbContext, UsersDbContext>();
            services.AddTransient<ILoggingMqContext, LoggingMqContext>();
            services.AddTransient<IEmailContext, EmailContext>();
            services.AddTransient<ICardsDbContext, CardsDbContext>();

            AuthenticatedEncryptorConfiguration configuration = new AuthenticatedEncryptorConfiguration()
            {
                EncryptionAlgorithm = Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.EncryptionAlgorithm.AES_256_CBC,
                ValidationAlgorithm = Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ValidationAlgorithm.HMACSHA512,
            };

            services.AddDataProtection()
                .PersistKeysToStackExchangeRedis(ConnectionMultiplexer.Connect(AppSettings.Current.RedisConnectionString), AppSettings.Current.RedisKey)
                .UseCryptographicAlgorithms(configuration: configuration);

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = AppSettings.Current.RedisConnectionString;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => 
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "digicard.api v1");
                    c.RoutePrefix = string.Empty;
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
