namespace digicard.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Options;
    using digicard.Components.API;
    using digicard.Components.API.Cards;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.Shared.API;
    using digicard.Components.Shared.Logging;
    using digicard.Mappers;
    using digicard.Models.API.Request.Cards;

    [Authorize(policy: "IsLoggedIn")]
    public class CardsController : DigicardBaseAPIController
    {
        public CardsController(IOptions<AppSettings> appSettings, IServiceProvider serviceProvider, IDistributedCache cache)
            : base(appSettings, serviceProvider, cache)
        {
        }

        [HttpGet]
        [Route("cards")]
        [AllowAnonymous]
        public async Task<APIResult> GetCards()
        {
            APIResult result = new APIResult { Data = null };
            string countStr = Request.Query.FirstOrDefault(k => k.Key.ToLower() == "count").Value;

            int count = int.TryParse(countStr, out count) ? count : 10;

            List<Card> cards = await Card.GetRandom(count);

            result.Data = CardsMapper.MapCardListToCardResponseModelList(cards);

            return result;
        }

        [HttpGet]
        [Route("accounts/{accountId}/cards")]
        public async Task<APIResult> GetCardsForAccount(string accountId)
        {
            APIResult result = new APIResult { Data = null };

            GetCardsByAccountIdRequestModel requestModel = new GetCardsByAccountIdRequestModel();
            
            try
            {
                requestModel.UrlParameters.Add("AccountId", accountId);

                await LogEntry.Write(this.Component, Method, requestModel.ToString(), userId: this.CurrentUser.UserId,  accountId: this.CurrentUser.CurrentAccount == null ? 0 : this.CurrentUser.CurrentAccount.AccountId);

                if (!await requestModel.Validate())
                {
                    result.Errors.AddRange(requestModel.Result.Errors);
                }
                else
                {
                    GetCardsByAccountIdRequestModelValidateResult validated = (GetCardsByAccountIdRequestModelValidateResult)requestModel.Result;

                    List<Card> cards = await Card.GetByAccountId(validated.Account.AccountId);

                    result.Data = CardsMapper.MapCardListToCardResponseModelList(cards);
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, this.Method, ex);
                result.Errors.Add(APIError.GetUnexpectedAPIError(new GetCardsByAccountIdRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            return result;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("cards/slug/{slug}")]
        public async Task<APIResult> GetCardBySlug(string slug)
        {
            APIResult result = new APIResult { Data = null };

            GetCardBySlugRequestModel requestModel = new GetCardBySlugRequestModel();
            
            try
            {
                requestModel.UrlParameters.Add("Slug", slug);

                await LogEntry.Write(this.Component, Method, requestModel.ToString(), userId: this.CurrentUser == null ? 0 : this.CurrentUser.UserId,  accountId: this.CurrentUser == null || this.CurrentUser.CurrentAccount == null ? 0 : this.CurrentUser.CurrentAccount.AccountId);

                if (!await requestModel.Validate())
                {
                    result.Errors.AddRange(requestModel.Result.Errors);
                }
                else
                {
                    GetCardBySlugRequestModelValidateResult validated = (GetCardBySlugRequestModelValidateResult)requestModel.Result;

                    result.Data = CardsMapper.MapCardToCardResponseModel(validated.Card);
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, this.Method, ex);
                result.Errors.Add(APIError.GetUnexpectedAPIError(new GetCardBySlugRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            return result;
        }


        [HttpPost]
        [Route("cards")]
        public async Task<APIResult> CreateCard([FromBody] CreateCardRequestModel requestModel)
        {
            APIResult result = new APIResult { Data = null };

            try
            {
                if (requestModel == null)
                {
                    result.Errors.Add(APIError.GetNullRequestModelError(new CreateCardRequestModel()));
                }
                else
                {
                    await LogEntry.Write(this.Component, this.Method, requestModel.ToString(), userId: this.CurrentUser.UserId, accountId: this.CurrentUser.CurrentAccount == null ? 0 : this.CurrentUser.CurrentAccount.AccountId);

                    if (!await requestModel.Validate())
                    {
                        result.Errors.AddRange(requestModel.Result.Errors);
                    }
                    else
                    {
                        CreateCardRequestModelValidateResult validated = (CreateCardRequestModelValidateResult)requestModel.Result;

                        Card card = new Card
                        {
                            UserId = this.CurrentUser.UserId,
                            NameOnCard = validated.NameOnCard,
                            Description = validated.Description,
                            Mobile = validated.Mobile,
                            Landline = validated.Landline,
                            Email = validated.Email,
                            Facebook = validated.Facebook,
                            Twitter = validated.Twitter,
                            Instagram = validated.Instagram,
                            Domain = validated.Domain,
                            AddressLine1 = validated.AddressLine1,
                            AddressLine2 = validated.AddressLine2,
                            AddressLine3 = validated.AddressLine3,
                            PostCode = validated.PostCode,
                            Country = validated.Country
                        };

                        await card.Save();

                        result.Data = CardsMapper.MapCardToCardResponseModel(card);
                    }
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, this.Method, ex);
                result.Errors.Add(APIError.GetUnexpectedAPIError(new CreateCardRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            return result;
        }

        [HttpPost]
        [Route("cards/{cardId}")]
        public async Task<APIResult> UpdateCard(string cardId)
        {
            APIResult result = new APIResult { Data = null };

            await Task.Delay(0);

            return result;
        }

        [HttpDelete]
        [Route("cards/{cardId}")]
        public async Task<APIResult> DeleteCard(string cardId)
        {
            APIResult result = new APIResult { Data = null };

            DeleteCardRequestModel requestModel = new DeleteCardRequestModel();

            try
            {
                requestModel.UrlParameters.Add("CardId", cardId);

                if (!await requestModel.Validate())
                {
                    result.Errors.AddRange(requestModel.Result.Errors);
                }
                else
                {
                    DeleteCardRequestModelValidateResult validated = (DeleteCardRequestModelValidateResult)requestModel.Result;

                    if (validated.Card != null)
                    {
                        await validated.Card.Delete();
                        result.Data = CardsMapper.MapCardToCardResponseModel(validated.Card);
                    }
                }
            }
            catch (Exception)
            {

            }
            await Task.Delay(0);

            return result;
        }

        [HttpPost]
        [Route("cards/publish/{cardId}")]
        public async Task<APIResult> PublishCard(string cardId)
        {
            APIResult result = new APIResult { Data = null };

            await Task.Delay(0);

            return result;
        }
    }
}
