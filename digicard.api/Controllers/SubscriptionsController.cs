﻿namespace digicard.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Options;
    using digicard.Components.API;
    using digicard.Components.Shared.API;

    [Authorize]
    public class SubscriptionsController : DigicardBaseAPIController
    {
        public SubscriptionsController(IOptions<AppSettings> appSettings, IServiceProvider serviceProvider, IDistributedCache cache)
            : base(appSettings, serviceProvider, cache)
        {
        }

        [HttpGet]
        public async Task<APIResult> GetBraintreeTokenForUser(string userId)
        {
            APIResult result = new APIResult { Data = null };

            await Task.Delay(0);

            return result;
        }

        [HttpPost]
        public async Task<APIResult> Update()
        {
            APIResult result = new APIResult { Data = null };

            await Task.Delay(0);

            // TODO(TS): Submit subscription detai
            return result;
        }

        [HttpPost]
        public async Task<APIResult> Cancel(string userId)
        {
            APIResult result = new APIResult { Data = null };

            await Task.Delay(0);

            // TODO(TS): Cancel subscription
            return result;
        }
    }
}
