﻿namespace digicard.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Options;
    using digicard.Components.API;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.Shared.Logging;
    using digicard.Components.API.Accounts;
    using digicard.Components.Shared.API;
    using digicard.Mappers;
    using digicard.Models.API.Request.Users;
    using digicard.Models.API.Request.Accounts;

    [Authorize(Policy = "IsLoggedIn")]
    public class AccountsController : DigicardBaseAPIController
    {
        public AccountsController(IOptions<AppSettings> appSettings, IServiceProvider serviceProvider, IDistributedCache cache)
            : base(appSettings, serviceProvider, cache)
        {
        }

        [HttpPost]
        [Route("accounts")]
        public async Task<APIResult> CreateAccount([FromBody]CreateAccountRequestModel requestModel)
        {
            APIResult result = new APIResult { Data = null };

            try
            {
                if (requestModel == null)
                {
                    result.Errors.Add(APIError.GetNullRequestModelError(new CreateAccountRequestModel()));
                }
                else
                {
                    await LogEntry.Write(this.Component, this.Method, requestModel.ToString(), userId: this.CurrentUser.UserId, accountId: this.CurrentUser.CurrentAccount == null ? 0 : this.CurrentUser.CurrentAccount.AccountId);

                    if (!await requestModel.Validate())
                    {
                        result.Errors.AddRange(requestModel.Result.Errors);
                    }
                    else
                    {
                        CreateAccountRequestModelValidateResult validated = (CreateAccountRequestModelValidateResult)requestModel.Result;

                        Account account = new Account
                        {
                            Name = validated.Name,
                        };

                        account.UserId = this.CurrentUser.UserId;

                        await account.Save();

                        result.Data = AccountsMapper.MapAccountToAccountResponseModel(account);
                    }
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, this.Method, ex);
                result.Errors.Add(APIError.GetUnexpectedAPIError(new CreateUserRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            await LogEntry.Write(this.Component, this.Method, result, userId: this.CurrentUser.UserId);

            return result;
        }
    }
}
