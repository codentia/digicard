﻿namespace digicard.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Options;
    using digicard.Components.API;
    using digicard.Components.Shared.API;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.Shared.Logging;
    using digicard.Components.API.Users;
    using digicard.Mappers;
    using digicard.Models.API.Request.Sessions;

    public class SessionsController : DigicardBaseAPIController
    {
        public SessionsController(IOptions<AppSettings> appSettings, IServiceProvider serviceProvider, IDistributedCache cache)
            : base(appSettings, serviceProvider, cache)
        {
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("sessions")]
        public async Task<APIResult> StartSession([FromBody]CreateSessionRequestModel requestModel)
        {
            APIResult result = new APIResult { Data = null };

            try
            {
                if (requestModel == null)
                {
                    result.Errors.Add(APIError.GetNullRequestModelError(new CreateSessionRequestModel()));
                }
                else
                {
                    await LogEntry.Write(this.Component, Method, requestModel.ToString());

                    if (!await requestModel.Validate())
                    {
                        result.Errors.AddRange(requestModel.Result.Errors);
                    }
                    else
                    {
                        CreateSessionRequestModelValidateResult validated = (CreateSessionRequestModelValidateResult)requestModel.Result;
                        Tuple<User, LoginResponseReason> authenticateResult = await digicard.Components.API.Users.User.Authenticate(validated.Username, validated.Password, validated.IPAddress);
                        await LogEntry.Write(this.Component, Method, $"CreateSessionRequestModel Authenticate result - {authenticateResult.Item2.ToString()}");
                        result.Data = UsersMapper.MapLoginDataToILoginResponse(authenticateResult.Item1);
                    }
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, Method, ErrorBuilder.ExceptionDiagnosticString(ex), 100);
                result.Errors.Add(APIError.GetUnexpectedAPIError(new CreateSessionRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            await LogEntry.Write(this.Component, Method, result);

            return result;
        }

        [HttpGet]
        [Route("sessions")]
        [Authorize(Policy = "IsLoggedIn")]
        public async Task<APIResult> RetrieveSession()
        {
            APIResult result = new APIResult { Data = null };

            RetrieveSessionRequestModel requestModel = new RetrieveSessionRequestModel();

            try
            {
                requestModel.UrlParameters.Add("Username", Request.Query.FirstOrDefault(k => k.Key.ToLower() == "username").Value);

                await LogEntry.Write(this.Component, Method, requestModel.ToString(),  userId: this.CurrentUser.UserId, accountId: this.CurrentUser.CurrentAccount == null ? 0 : this.CurrentUser.CurrentAccount.AccountId);

                if (!await requestModel.Validate())
                {
                    result.Errors.AddRange(requestModel.Result.Errors);
                }
                else
                {
                    RetrieveSessionRequestModelValidateResult validated = (RetrieveSessionRequestModelValidateResult)requestModel.Result;

                    User user = await digicard.Components.API.Users.User.Get(validated.Username);
                    result.Data = UsersMapper.MapUserToIUserResponse(user);
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, Method, ErrorBuilder.ExceptionDiagnosticString(ex), severity: 100, userId: this.CurrentUser.UserId, accountId: this.CurrentUser.CurrentAccount == null ? 0 : this.CurrentUser.CurrentAccount.AccountId);
                result.Errors.Add(APIError.GetUnexpectedAPIError(new RetrieveSessionRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            await LogEntry.Write(this.Component, Method, result, userId: this.CurrentUser.UserId,  accountId: this.CurrentUser.CurrentAccount == null ? 0 : this.CurrentUser.CurrentAccount.AccountId);

            return result;
        }

        [HttpDelete]
        [Route("sessions")]
        [Authorize(Policy = "IsLoggedIn")]
        public async Task<APIResult> DeleteSession()
        {
            APIResult result = new APIResult { Data = null };

            DeleteSessionRequestModel requestModel = new DeleteSessionRequestModel();

            try
            {
                requestModel.UrlParameters.Add("Username", Request.Query.FirstOrDefault(k => k.Key.ToLower() == "username").Value);

                await LogEntry.Write(this.Component, Method, requestModel.ToString());

                if (!await requestModel.Validate())
                {
                    result.Errors.AddRange(requestModel.Result.Errors);
                }
                else
                {
                    DeleteSessionRequestModelValidateResult validated = (DeleteSessionRequestModelValidateResult)requestModel.Result;

                    User user = await digicard.Components.API.Users.User.Get(validated.Username);
                    user.GenerateSecurityToken(invalidatingToken: true);

                    result.Data = true;
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, Method, ErrorBuilder.ExceptionDiagnosticString(ex), 100);
                result.Errors.Add(APIError.GetUnexpectedAPIError(new RetrieveSessionRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            await LogEntry.Write(this.Component, Method, result);

            return result;
        }
    }
}
