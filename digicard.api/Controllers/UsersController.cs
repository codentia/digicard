﻿namespace digicard.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Options;
    using digicard.Components.API;
    using digicard.Components.API.ErrorCodes;
    using digicard.Components.API.Authentication;
    using digicard.Components.Shared.Logging;
    using digicard.Components.Shared.API;
    using digicard.Components.API.Users;
    using digicard.Mappers;
    using digicard.Models.API.Request.Users;

    public class UsersController : DigicardBaseAPIController
    {
        public UsersController(IOptions<AppSettings> appSettings, IServiceProvider serviceProvider, IDistributedCache cache)
            : base(appSettings, serviceProvider, cache)
        {
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("/users")]
        public async Task<APIResult> CreateUser([FromBody] CreateUserRequestModel requestModel)
        {
            APIResult result = new APIResult { Data = null };

            try
            {
                if (requestModel == null)
                {
                    result.Errors.Add(APIError.GetNullRequestModelError(new CreateUserRequestModel()));
                }
                else
                {
                    await LogEntry.Write(this.Component, Method, requestModel.ToString());

                    if (!await requestModel.Validate())
                    {
                        result.Errors.AddRange(requestModel.Result.Errors);
                    }
                    else
                    {
                        CreateUserRequestModelValidateResult validated = (CreateUserRequestModelValidateResult)requestModel.Result;
                        User user = new User
                        {
                            EmailAddress = validated.EmailAddress,
                        };

                        await user.FinishPasswordReset(validated.Password);
                        await user.Save();

                        result.Data = UsersMapper.MapUserToIUserResponse(user);
                    }
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, Method, ErrorBuilder.ExceptionDiagnosticString(ex), severity: 100);
                result.Errors.Add(APIError.GetUnexpectedAPIError(new CreateUserRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            await LogEntry.Write(this.Component, Method, result);

            return result;
        }

        [HttpGet]
        [Route("users/{id}")]
        [Authorize(Policy = "IsLoggedIn")]
        public async Task<APIResult> GetUser(string id)
        {
            APIResult result = new APIResult { Data = null };

            GetUserRequestModel requestModel = new GetUserRequestModel();

            try
            {
                requestModel.UrlParameters.Add("UserId", id);

                await LogEntry.Write(this.Component, Method, requestModel.ToString(), userId: this.CurrentUser.UserId,  accountId: this.CurrentUser.CurrentAccount == null ? 0 : this.CurrentUser.CurrentAccount.AccountId);

                if (!await requestModel.Validate())
                {
                    result.Errors.AddRange(requestModel.Result.Errors);
                }
                else
                {
                    GetUserRequestModelValidateResult validated = (GetUserRequestModelValidateResult)requestModel.Result;
                    result.Data = UsersMapper.MapUserToIUserResponse(validated.User);
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, Method, ErrorBuilder.ExceptionDiagnosticString(ex), severity: 100, userId: this.CurrentUser.UserId, accountId: this.CurrentUser.CurrentAccount == null ? 0 : this.CurrentUser.CurrentAccount.AccountId);
                result.Errors.Add(APIError.GetUnexpectedAPIError(new GetUserRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            await LogEntry.Write(this.Component, Method, result, userId: this.CurrentUser.UserId, accountId: this.CurrentUser.CurrentAccount == null ? 0 : this.CurrentUser.CurrentAccount.AccountId);

            return result;
        }

        [HttpPost]
        [Route("users/reset")]
        [AllowAnonymous]
        public async Task<APIResult> StartPasswordReset([FromBody] StartPasswordResetRequestModel requestModel)
        {
            const string Method = "StartPasswordReset";

            APIResult result = new APIResult { Data = false };

            try
            {
                if (requestModel == null)
                {
                    result.Errors.Add(APIError.GetNullRequestModelError(new StartPasswordResetRequestModel()));
                }
                else
                {
                    await LogEntry.Write(this.Component, Method, requestModel.ToString());

                    await requestModel.Validate();

                    if (requestModel.Result.Errors.Count > 0)
                    {
                        result.Errors.AddRange(requestModel.Result.Errors);
                    }
                    else
                    {
                        StartPasswordResetRequestModelValidateResult validated = (StartPasswordResetRequestModelValidateResult)requestModel.Result;

                        await validated.User.StartPasswordReset();
                        result.Data = true;
                    }
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, Method, ErrorBuilder.ExceptionDiagnosticString(ex), 100);

                result.Errors.Add(APIError.GetUnexpectedAPIError(new StartPasswordResetRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            await LogEntry.Write(this.Component, Method, result);

            return result;
        }

        [HttpPut]
        [Route("users/reset/{token}")]
        [AllowAnonymous]
        public async Task<APIResult> FinishPasswordReset(string token, [FromBody] FinishPasswordResetRequestModel requestModel)
        {
            const string Method = "FinishPasswordReset";

            APIResult result = new APIResult { Data = false };

            try
            {
                if (requestModel == null)
                {
                    result.Errors.Add(APIError.GetNullRequestModelError(new FinishPasswordResetRequestModel()));
                }
                else
                {
                    requestModel.Token = token;
                    await requestModel.Validate();

                    if (requestModel.Result.Errors.Count > 0)
                    {
                        result.Errors.AddRange(requestModel.Result.Errors);
                    }
                    else
                    {
                        FinishPasswordResetRequestModelValidateResult validated = (FinishPasswordResetRequestModelValidateResult)requestModel.Result;

                        await validated.User.FinishPasswordReset(validated.Token, validated.PasswordHash);
                        result.Data = true;
                    }
                }
            }
            catch (Exception ex)
            {
                await LogEntry.Write(this.Component, Method, ErrorBuilder.ExceptionDiagnosticString(ex), 100);

                result.Errors.Add(APIError.GetUnexpectedAPIError(new FinishPasswordResetRequestModel(), ErrorBuilder.ExceptionDiagnosticString(ex)));
            }

            await LogEntry.Write(this.Component, Method, result);

            return result;
        }
    }
}
