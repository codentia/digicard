﻿namespace digicard.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Options;
    using digicard.Components.API;
    using digicard.Components.Shared.API;

    [AllowAnonymous]
    public class WebhooksController : DigicardBaseAPIController
    {
        public WebhooksController(IOptions<AppSettings> appSettings, IServiceProvider serviceProvider, IDistributedCache cache)
            : base(appSettings, serviceProvider, cache)
        {
        }

        [HttpPost]
        [Route("/external/braintree")]
        public async Task<IActionResult> Braintree()
        {
            await Task.Delay(0);
            // TODO(TS): Handle Braintree webhook
            return Ok();
        }
    }
}
